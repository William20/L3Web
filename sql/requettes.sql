 CREATE TABLE IF NOT EXISTS Inscription (identifiant VARCHAR(20) NOT NULL PRIMARY KEY,
                                                    email VARCHAR(50) NOT NULL,
                                                    mdp VARCHAR(100) NOT NULL,
                                                    cle_activation VARCHAR(20) NOT NULL,
                                                    compte_actif INT(1) NOT NULL,
                                                    voiture VARCHAR(3) NOT NULL,
                                                    place INT(100) NOT NULL,
                                                    prix INT(100) NOT NULL ,
                                                    Dormir VARCHAR(3) NOT NULL,
                                                    placeD INT(100) NOT NULL,
                                                    prixD INT(100) NOT NULL
                                                    );
CREATE TABLE IF NOT EXISTS Evenement
                                  (nom VARCHAR(50) NOT NULL PRIMARY KEY,
                                  lieu VARCHAR(500) NOT NULL,
                                  description VARCHAR(500) NOT NULL,
                                  date_evenement DATE NOT NULL,
                                  participant VARCHAR(500) NOT NULL,
                                  admin VARCHAR(500) NOT NULL,
                                  visibilite VARCHAR (10) NOT NULL,
                                  budget VARCHAR (3) NOT NULL,
                                  voiture VARCHAR (3) NOT NULL,
                                  Dormir VARCHAR (3) NOT NULL,
                                  compteur INT (200) NOT NULL);

CREATE TABLE IF NOT EXISTS Voiture
                                  (Id VARCHAR (100) NOT NULL PRIMARY KEY,
                                  evenement VARCHAR(50) NOT NULL,
                                  conducteur VARCHAR(50) NOT NULL,
                                  passagers VARCHAR(500) NOT NULL,
                                  lieuRdv VARCHAR(500) NOT NULL);

CREATE TABLE IF NOT EXISTS Dormir
                                  (Id VARCHAR (100) NOT NULL PRIMARY KEY,
                                  evenement VARCHAR(50) NOT NULL,
                                  hebergeur VARCHAR(50) NOT NULL,
                                  gens VARCHAR(500) NOT NULL);
CREATE TABLE IF NOT EXISTS Budget
                                  (Id VARCHAR (100) NOT NULL PRIMARY KEY,
                                  evenement VARCHAR(50) NOT NULL,
                                  produit VARCHAR(500) NOT NULL,
                                  prise VARCHAR(500) NOT NULL,
                                  prix INTEGER NOT NULL,
                                  idRep INTEGER NOT NULL);