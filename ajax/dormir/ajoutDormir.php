<?php
/**
 * Created by PhpStorm.
 * User: amelie
 * Date: 17/11/16
 * Time: 21:30
 */
require_once("../../include/fonctions.inc.php");

if (isset($_POST["Nom_ED"]) && isset($_POST["hebergeur"]) && isset($_POST["gens"]) && isset($_POST["Prix"]) && !isset($_POST["DelD"])) {
    $conn = connexion_SQL("../../");
    $name = $_POST["Nom_ED"];
    $hebergeur = $_POST["hebergeur"];
    $gens = $_POST["gens"];
    $result = $conn->query("SELECT gens FROM Dormir WHERE (hebergeur LIKE BINARY \"$hebergeur\" AND evenement = \"$name\" )");
    if($result==false || $result->num_rows<=0) {
        {
            $result = $conn->query("SELECT Id FROM Dormir ");
            if($result != false && $result->num_rows>0) {
                $id = end($result->fetch_all())[0];
                $id++;
            } else {
                $id=0;
            }
            $g = serialize(array($gens));
            $conn->query("INSERT INTO Dormir (Id,evenement,hebergeur,gens) VALUES ('$id','$name','$hebergeur','$gens')");
        }
    } else {
        $passagers = $result->fetch_row()[0];
        if (unserialize($passagers) != array()) {
            if (appartient(unserialize($passagers), $gens) != 0) {
                $tmp = unserialize($passagers);
                array_push($tmp, $gens);
            }
        } else {
            $tmp = array("$gens");
        }
        $tmp = serialize($tmp);
        $conn->query("UPDATE Dormir SET gens = '$tmp' WHERE (hebergeur LIKE BINARY \"$hebergeur\" AND evenement = \"$name\" )");
    }
    $conn->close();
}

if (isset($_POST["DelD"]) && isset($_POST["gens"])) {
    $conn = connexion_SQL("../../");
    $name = $_POST["Nom_ED"];
    $hebergeur = $_POST["hebergeur"];
    $gens = $_POST["gens"];
    $result = $conn->query("SELECT gens FROM Dormir WHERE (hebergeur LIKE BINARY \"$hebergeur\" AND evenement = \"$name\" )");
    $passagers = $result->fetch_row()[0];
    $array2 = unserialize($passagers);
    $tmp = array();
    foreach ($array2 as $elemnt) {
        if ($elemnt != $gens) {
            array_push($tmp, $elemnt);
        }
    }
    $tmp = serialize($tmp);
    $conn->query("UPDATE Dormir SET gens = '$tmp' WHERE (hebergeur LIKE BINARY \"$hebergeur\" AND evenement = \"$name\" )");
    $conn->close();
}
?>