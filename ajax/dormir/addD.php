<?php
/**
 * Created by PhpStorm.
 * User: amelie
 * Date: 17/11/16
 * Time: 22:31
 */

require_once("../../include/fonctions.inc.php");

if (isset($_POST["NomEvt"]) && isset($_POST["suppD"])) {
    $conn = connexion_SQL("../../");
    $valeurNom = $_POST["NomEvt"];
    $conn->query("UPDATE Evenement set Dormir ='Oui' WHERE nom=\"$valeurNom\" ");
    $result = $conn->query("SELECT participant FROM Evenement WHERE nom =\"$valeurNom\" ");
    $participant = unserialize($result->fetch_array(MYSQLI_NUM)[0]);
    echo '<div class="form-group" id=dormirDiv>';
    ?>
    <label for="budget">Gestion de l'hebergement :</label>
    <?php
    $savoir_siDormir = 0;
    foreach ($participant as $personne) {
        $result = $conn->query("SELECT Dormir FROM Inscription WHERE identifiant LIKE BINARY '$personne' ");
        $resultDormir = $result->fetch_array(MYSQLI_NUM)[0];
        if ($resultDormir == "Oui") {
            $savoir_siDormir = 1;
            ?>
            <h4>Qui dort chez <?php echo $personne; ?> ?</h4>
            <h5>Cliquez sur une case pour vous inscrire ou vous désinscrire (vous ne pouvez pas vous
                inscrire
                dans votre
                propre
                voiture mais vous pouvez supprimer des utilisateurs de votre tableau)</h5>
            <?php
            $resultprixPlace = $conn->query("SELECT prixD, placeD FROM Inscription WHERE identifiant LIKE BINARY '$personne' ");
            $prixPlace = $resultprixPlace->fetch_array(MYSQLI_NUM);
            $prix = $prixPlace[0];
            $place = $prixPlace[1];
            $result2 = $conn->query("SELECT gens FROM Dormir WHERE (hebergeur LIKE BINARY '$personne' AND evenement = \"$valeurNom\") ");
            $passagersRdv = $result2->fetch_row();
            $array2 = unserialize($passagersRdv[0]);
            echo '<h2 style="margin-top: 2%; font-size: 120%; font-weight: bold"> Le prix par personne et par nuit est de  ' . $prix . ' euros pour cet hébergeur';
            $table = "Tablevoiture";
            if ($personne == $_COOKIE['utilisateur'])
                $table = "Tablevoiture hoverNon";
            echo '<table class="' . $table . '">';
            if (intval(($place / 2)) == (($place - 1) / 2)) {
                for ($i = 1; $i < ($place) - 1;) {
                    $j = $i + 1;
                    $id1 = $personne . "D_" . $i;
                    $id2 = $personne . "D_" . $j;
                    if ($personne == $_COOKIE['utilisateur']) {
                        $onclick = "virerDormir('" . $personne . "',this.id);";
                        $onclick2 = $onclick;
                    } else {
                        $onclick = "ajoutDormir('" . $personne . "D_" . $i . "','" . $_COOKIE["utilisateur"] . "','" . $personne . "','" . $place . "','" . $prix . "')";
                        $onclick2 = "ajoutDormir('" . $personne . "D_" . $j . "','" . $_COOKIE["utilisateur"] . "','" . $personne . "','" . $place . "','" . $prix . "')";

                    }
                    $j = $i + 1;
                    ?>
                    <tr id="<?php echo $personne; ?>">
                        <td class="tdthvoiture" id='<?php echo $id1; ?>'
                            onclick="<? echo $onclick; ?>"><?php if (isset($array2[$i - 1])) echo $array2[$i - 1] ?></td>
                        <td class="tdthvoiture" id='<?php echo $id2; ?>'
                            onclick="<? echo $onclick; ?>"><?php if (isset($array2[$i])) echo $array2[$i] ?></td>
                    </tr>
                    <?php
                    $i = $i + 2;
                }
                $onclick3 = "ajoutDormir('" . $personne . "D_" . $place . "','" . $_COOKIE["utilisateur"] . "','" . $personne . "','" . $place . "','" . $prix . "')";
                ?>

                <tr id="<?php echo $personne; ?>">
                    <td class="tdthDoiture" colspan="2" id='<?php echo $personne . 'D_' . $place; ?>'
                        onclick="<? echo $onclick3; ?>"><?php if (isset($array2[$place - 1])) echo $array2[$place - 1] ?></td>
                </tr>
                <?php
            } else {
                for ($i = 1; $i < $place;) {
                    $j = $i + 1;
                    $id1 = $personne . "D_" . $i;
                    $id2 = $personne . "D_" . $j;
                    if ($personne == $_COOKIE['utilisateur']) {
                        $onclick = "virerDormir('" . $personne . "',this.id);";
                        $onclick2 = $onclick;
                    } else {
                        $onclick = "ajoutDormir('" . $personne . "D_" . $i . "','" . $_COOKIE["utilisateur"] . "','" . $personne . "','" . $place . "','" . $prix . "')";
                        $onclick2 = "ajoutDormir('" . $personne . "D_" . $j . "','" . $_COOKIE["utilisateur"] . "','" . $personne . "','" . $place . "','" . $prix . "')";

                    }
                    ?>
                    <tr id="<?php echo $personne; ?>">
                        <td class="tdthvoiture" id='<?php echo $id1; ?>'
                            onclick="<? echo $onclick; ?>"><?php if (isset($array2[$i - 1])) echo $array2[$i - 1] ?></td>
                        <td class="tdthvoiture" id='<?php echo $id2; ?>'
                            onclick="<? echo $onclick2; ?>"><?php if (isset($array2[$i])) echo $array2[$i] ?></td>
                    </tr>

                    <?php
                    $i = $i + 2;
                }
            }
            echo '</table>';
        }
    }
    if ($savoir_siDormir == 0) {
        echo "<p>Aucun participant n'a la possibilité d'heberger</p>";
    }
    echo '<input class="btn btn-default" onclick="SuppD();" type="button" name="suppD" id="suppD"
                       value="Supprimer l\'option "/>';
    echo '</div>';
}
?>