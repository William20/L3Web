<?php
/**
 * Created by PhpStorm.
 * User: amelie
 * Date: 17/11/16
 * Time: 21:30
 */
require_once("../../include/fonctions.inc.php");

if (isset($_POST["Nom_E"]) && isset($_POST["Conducteur"]) && isset($_POST["Passager"]) && isset($_POST["Prix"]) && !isset($_POST["Del"])) {
    $conn = connexion_SQL("../../");
    $name = $_POST["Nom_E"];
    $conducteur = $_POST["Conducteur"];
    $passager = $_POST["Passager"];
    $result = $conn->query("SELECT passagers FROM Voiture WHERE (conducteur LIKE BINARY \"$conducteur\" AND evenement = \"$name\" )");
    if ($result == false || $result->num_rows <= 0) {
        {
            $result = $conn->query("SELECT Id FROM Voiture ");
            if ($result != false && $result->num_rows > 0) {
                $id = end($result->fetch_all())[0];
                $id++;
            } else {
                $id = 0;
            }
            $pass = serialize(array($passager));
            $conn->query("INSERT INTO Voiture (Id,evenement,conducteur,passagers,lieuRdv) VALUES ('$id','$name','$conducteur','$pass','')");
        }
    } else {
        $passagers = $result->fetch_row()[0];
        if (unserialize($passagers) != array()) {
            if (appartient(unserialize($passagers), $passager) != 0) {
                $tmp = unserialize($passagers);
                array_push($tmp, $passager);
            }
        } else {
            $tmp = array("$passager");
        }
        $tmp = serialize($tmp);
        $conn->query("UPDATE Voiture SET passagers = '$tmp' WHERE (conducteur LIKE BINARY \"$conducteur\" AND evenement = \"$name\" )");
        $conn->close();
    }
}

if (isset($_POST["Del"]) && isset($_POST["Passager"])) {
    $conn = connexion_SQL("../../");
    $name = $_POST["Nom_E"];
    $conducteur = $_POST["Conducteur"];
    $passager = $_POST["Passager"];
    $result = $conn->query("SELECT passagers FROM Voiture WHERE (conducteur LIKE BINARY \"$conducteur\" AND evenement = \"$name\" )");
    $passagers = $result->fetch_row()[0];
    $array2 = unserialize($passagers);
    $tmp = array();
    foreach ($array2 as $elemnt) {
        if ($elemnt != $passager) {
            array_push($tmp, $elemnt);
        }
    }
    $tmp = serialize($tmp);
    $conn->query("UPDATE Voiture SET passagers = '$tmp' WHERE (conducteur LIKE BINARY \"$conducteur\" AND evenement = \"$name\" )");
    $conn->close();
}
?>