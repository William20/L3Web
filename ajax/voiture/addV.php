<?php
/**
 * Created by PhpStorm.
 * User: amelie
 * Date: 17/11/16
 * Time: 22:30
 */

require_once("../../include/fonctions.inc.php");

if (isset($_POST["NomEvt"]) && isset($_POST["suppV"])) {
    $conn = connexion_SQL("../../");
    $valeurNom = $_POST["NomEvt"];
    $conn->query("UPDATE Evenement set voiture ='Oui' WHERE nom=\"$valeurNom\" ");
    $result = $conn->query("SELECT participant FROM Evenement WHERE nom =\"$valeurNom\" ");
    $participant = unserialize($result->fetch_array(MYSQLI_NUM)[0]);
    ?>
    <div class="form-group" id="voitureDiv">
        <label for="budget">Gestion du covoiturage :</label>
        <?php
        $savoir_siVoiture = 0;
        foreach ($participant as $personne) {
            $result = $conn->query("SELECT voiture FROM Inscription WHERE identifiant LIKE BINARY '$personne' ");
            $resultVoiture = $result->fetch_array(MYSQLI_NUM)[0];
            if ($resultVoiture == "Oui") {
                $savoir_siVoiture = 1;
                ?>
                <h4>Passagers dans voiture de <?php echo $personne; ?></h4>
                <h5>Cliquez sur une case pour vous inscrire ou vous désinscrire (vous ne pouvez pas vous
                    inscrire
                    dans votre
                    propre
                    voiture mais vous pouvez supprimer des utilisateurs de votre tableau)</h5>
                <?php
                $resultprixPlace = $conn->query("SELECT prix, place FROM Inscription WHERE identifiant LIKE BINARY '$personne' ");
                $prixPlace = $resultprixPlace->fetch_array(MYSQLI_NUM);
                $prix = $prixPlace[0];
                $place = $prixPlace[1];
                $result2 = $conn->query("SELECT passagers, lieuRdv FROM Voiture WHERE (conducteur LIKE BINARY '$personne' AND evenement = \"$valeurNom\") ");
                $passagersRdv = $result2->fetch_row();
                $array2 = unserialize($passagersRdv[0]);
                $lieuRDV = $passagersRdv[1];
                echo '<h2 style="margin-top: 2%; font-size: 120%; font-weight: bold"> Le prix par personne et par trajet moyen (environ 15min de temps de trajet) est de ' . $prix . ' euros pour cette voiture';
                echo '<h2 style="margin-top: 2%; font-size: 120%; font-weight: bold"> Le lieu de rendez-vous est : ' . $lieuRDV;
                $table = "Tablevoiture";
                if ($personne == $_COOKIE['utilisateur'])
                    $table = "Tablevoiture hoverNon";
                echo '<table class="' . $table . '">';
                if (intval(($place / 2)) == (($place - 1) / 2)) {
                    for ($i = 1; $i < ($place) - 1;) {
                        $j = $i + 1;
                        $id1 = $personne . "V_" . $i;
                        $id2 = $personne . "V_" . $j;
                        if ($personne == $_COOKIE['utilisateur']) {
                            $onclick = "virerVoiture('" . $personne . "',this.id);";
                            $onclick2 = $onclick;
                        } else {
                            $onclick = "ajoutVoiture('" . $personne . "V_" . $i . "','" . $_COOKIE["utilisateur"] . "','" . $personne . "','" . $place . "','" . $prix . "')";
                            $onclick2 = "ajoutVoiture('" . $personne . "V_" . $j . "','" . $_COOKIE["utilisateur"] . "','" . $personne . "','" . $place . "','" . $prix . "')";

                        }
                        $j = $i + 1;
                        ?>
                        <tr id="<?php echo $personne; ?>">
                            <td class="tdthvoiture" id='<?php echo $id1; ?>'
                                onclick="<? echo $onclick; ?>"><?php if (isset($array2[$i - 1])) echo $array2[$i - 1] ?></td>
                            <td class="tdthvoiture" id='<?php echo $id2; ?>'
                                onclick="<? echo $onclick; ?>"><?php if (isset($array2[$i])) echo $array2[$i] ?></td>
                        </tr>
                        <?php
                        $i = $i + 2;
                    }
                    $onclick3 = "ajoutVoiture('" . $personne . "V_" . $place . "','" . $_COOKIE["utilisateur"] . "','" . $personne . "','" . $place . "','" . $prix . "')";
                    ?>

                    <tr id="<?php echo $personne; ?>">
                        <td class="tdthvoiture" colspan="2" id='<?php echo $personne . 'V_' . $place; ?>'
                            onclick="<? echo $onclick3; ?>"><?php if (isset($array2[$place - 1])) echo $array2[$place - 1] ?></td>
                    </tr>
                    <?php
                } else {
                    for ($i = 1; $i < $place;) {
                        $j = $i + 1;
                        $id1 = $personne . "V_" . $i;
                        $id2 = $personne . "V_" . $j;
                        if ($personne == $_COOKIE['utilisateur']) {
                            $onclick = "virerVoiture('" . $personne . "',this.id);";
                            $onclick2 = $onclick;
                        } else {
                            $onclick = "ajoutVoiture('" . $personne . "V_" . $i . "','" . $_COOKIE["utilisateur"] . "','" . $personne . "','" . $place . "','" . $prix . "')";
                            $onclick2 = "ajoutVoiture('" . $personne . "V_" . $j . "','" . $_COOKIE["utilisateur"] . "','" . $personne . "','" . $place . "','" . $prix . "')";

                        }
                        ?>
                        <tr id="<?php echo $personne; ?>">
                            <td class="tdthvoiture" id='<?php echo $id1; ?>'
                                onclick="<? echo $onclick; ?>"><?php if (isset($array2[$i - 1])) echo $array2[$i - 1] ?></td>
                            <td class="tdthvoiture" id='<?php echo $id2; ?>'
                                onclick="<? echo $onclick2; ?>"><?php if (isset($array2[$i])) echo $array2[$i] ?></td>
                        </tr>

                        <?php
                        $i = $i + 2;
                    }
                }
                echo '</table>';
                if ($personne == $_COOKIE['utilisateur']) {
                    ?>
                    <form method="post" action="../traitement/modificationRDV.php">
                        <input type="text" name="NomUti" hidden value="<? echo $personne; ?>">
                        <label for="lieu_rdv_id"> Donnez un lieu de rendez-vous :</label>
                        <textarea class="form-control"
                                  placeholder="Exemple : RDV a 18h au 2 avenue Adolphe Chauvin"
                                  name="rdv" id="lieu_rdv_id" maxlength="300"
                                  rows="3"
                                  cols="30"
                                  required><?php if (isset($lieuRDV)) echo $lieuRDV; ?></textarea>
                        <input type="submit" value="Modifier mon lieu de rdv">
                    </form>
                    <?php
                }
            }
        }
        if ($savoir_siVoiture == 0) {
            echo "<p>Aucun participant n'a la possibilité d'utiliser une voiture</p>";
        }
        ?>
        <input class="btn-default btn" onclick="SuppV();" type="button" name="suppV" id="suppV"
               value="Supprimer l'option "/>
    </div>
    <?php
}
?>