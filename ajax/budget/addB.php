<?php
/**
 * Created by PhpStorm.
 * User: amelie
 * Date: 17/11/16
 * Time: 22:29
 */

require_once("../../include/fonctions.inc.php");

if (isset($_POST["NomEvt"]) && isset($_POST["supp"])) {
    $conn = connexion_SQL("../../");
    $valeurNom = $_POST["NomEvt"];
    $conn->query("UPDATE Evenement set budget ='Oui' WHERE nom=\"$valeurNom\" ");
    $result = $conn->query("SELECT participant FROM Evenement WHERE nom =\"$valeurNom\" ");
    $participant = unserialize($result->fetch_array(MYSQLI_NUM)[0]);
    ?>
    <div class="form-group" id=budgetDiv>
        <label for="budget">Gestion du budget :</label>
        <table id="budget">
            <tr>
                <th class="tdthBudget">Produit</th>
                <th class="tdthBudget">Prise en charge</th>
                <th class="tdthBudget">Coût (en euros)</th>
            </tr>
            <?php
            $resultB = $conn->query("SELECT * FROM `Budget` WHERE evenement = \"$valeurNom\"");
            if ($resultB == true && $resultB->num_rows > 0) {
                $ifor = 1;
                $pas = $resultB->num_rows;
            } else {
                $pas = 1;
                $ifor = 0;
                $valueB = array("", "", "");
                $placeholder1 = "Bouteille d'eau";
                $placeholder2 = "Jean";
                $placeholder3 = "2";
            }
            for ($i = 1; $i <= $pas; $i++) {
                if ($ifor) {
                    $value = $conn->query("SELECT produit, prise, prix FROM Budget WHERE evenement = \"$valeurNom\" ");
                    $valueB = $value->fetch_row();
                    $placeholder1 = "";
                    $placeholder2 = "";
                    $placeholder3 = "";
                }
                ?>
                <tr id="<?php echo $i; ?>">
                    <td>
                        <input onblur="addObjt(this);"
                               value="<?php echo $valueB[0]; ?>"
                               type="text" id="<?php echo $i; ?>"
                               name="objet<?php echo $i; ?>"
                               placeholder="<?php echo $placeholder1; ?>" required/>
                    </td>
                    <td>
                        <input onblur="addPrise(this);"
                               value="<?php echo $valueB[1]; ?>"
                               type="text" id="<?php echo $i; ?>"
                               name="prise<?php echo $i; ?>"
                               placeholder="<?php echo $placeholder2; ?>"/>
                    </td>
                    <td>
                        <input onblur="addPrix(this);"
                               value="<?php echo $valueB[2]; ?>"
                               type="number" id="<?php echo $i; ?>"
                               name="prix<?php echo $i; ?>"
                               placeholder="<?php echo $placeholder1; ?>"/>
                    </td>
                </tr>
                <?php
            }
            ?>
        </table>
        <input id='+' onclick='addLigneB(<?php echo $pas ?>);' type='button' value='+'/>
        <input id='fonction_a_changer' onclick='suppLigneB(<?php echo $resultB->num_rows ?>);' type='button'
               value='-'/>
        <br>
        <input class="btn btn-default" onclick="SuppB();" type="button" name="suppB" id="suppB"
               value="Supprimer l'option "/>
    </div>
    <?php
}

?>