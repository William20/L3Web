<?php
/**
 * Created by PhpStorm.
 * User: william
 * Date: 14/10/16
 * Time: 15:53
 */
ini_set('display_errors', 'off');

$serveur = "";
$nom = "";
$utilisateur = "";
$mdp = "";

if (isset($_POST['serveur'])) {
    if (isset($_POST['nom'])) {
        if (isset($_POST['utilisateur'])) {
            if (isset($_POST['mdp'])) {
                if (isset($_POST['submit'])) {
                    $serveur = $_POST['serveur'];
                    $nom = $_POST['nom'];
                    $utilisateur = $_POST['utilisateur'];
                    $mdp = $_POST['mdp'];
                    if (isset($_POST['oui_mail'])) {
                        $mail = "OUI";
                    } else {
                        $mail = "NON";
                    }
                    if (isset($_POST['port']) && !empty($_POST['port'])) {
                        $port = $_POST['port'];
                    } else {
                        $port = "3306";
                    }
                    $conn = new mysqli($serveur, $utilisateur, $mdp);
                    if ($conn->connect_errno || $conn->connect_error) {
                        $message = "Erreur d'accès aux serveurs, veuillez vérifier les informations rentrées.";
                        header("Location: ../admin/install.php?pb=$message");
                    } else {
                        $query1 = "CREATE DATABASE IF NOT EXISTS $nom";
                        $query2 = "CREATE TABLE IF NOT EXISTS Inscription (identifiant VARCHAR(20) NOT NULL PRIMARY KEY,
                                                    email VARCHAR(50) NOT NULL,
                                                    mdp VARCHAR(100) NOT NULL,
                                                    cle_activation VARCHAR(20) NOT NULL,
                                                    compte_actif INT(1) NOT NULL,
                                                    voiture VARCHAR(3) NOT NULL,
                                                    place INT(100) NOT NULL,
                                                    prix INT(100) NOT NULL ,
                                                    Dormir VARCHAR(3) NOT NULL,
                                                    placeD INT(100) NOT NULL,
                                                    prixD INT(100) NOT NULL
                                                    )";
                        $query3 = "CREATE TABLE IF NOT EXISTS Evenement 
                                  (nom VARCHAR(50) NOT NULL PRIMARY KEY, 
                                  lieu VARCHAR(500) NOT NULL, 
                                  description VARCHAR(500) NOT NULL, 
                                  date_evenement DATE NOT NULL, 
                                  participant VARCHAR(500) NOT NULL, 
                                  admin VARCHAR(500) NOT NULL, 
                                  visibilite VARCHAR (10) NOT NULL, 
                                  budget VARCHAR (3) NOT NULL,  
                                  voiture VARCHAR (3) NOT NULL, 
                                  Dormir VARCHAR (3) NOT NULL, 
                                  compteur INT (200) NOT NULL)";
                        $query4 = "CREATE TABLE IF NOT EXISTS Voiture 
                                  (Id VARCHAR (100) NOT NULL PRIMARY KEY, 
                                  evenement VARCHAR(50) NOT NULL,
                                  conducteur VARCHAR(50) NOT NULL, 
                                  passagers VARCHAR(500) NOT NULL, 
                                  lieuRdv VARCHAR(500) NOT NULL)";
                        $query5 = "CREATE TABLE IF NOT EXISTS Dormir
                                  (Id VARCHAR (100) NOT NULL PRIMARY KEY, 
                                  evenement VARCHAR(50) NOT NULL,
                                  hebergeur VARCHAR(50) NOT NULL, 
                                  gens VARCHAR(500) NOT NULL)";
                        $query6 = "CREATE TABLE IF NOT EXISTS Budget
                                  (Id VARCHAR (100) NOT NULL PRIMARY KEY, 
                                  evenement VARCHAR(50) NOT NULL, 
                                  produit VARCHAR(500) NOT NULL, 
                                  prise VARCHAR(500) NOT NULL, 
                                  prix INTEGER NOT NULL,
                                  idRep INTEGER NOT NULL)"; //TODO script d'install à partir du fichier sql

                        $test_connexion = "SELECT * FROM `Inscription`";
//                        echo $test_connexion;

                        $conn->query($query1);
                        $conn->select_db($nom);
                        $conn->query($query2);
                        $conn->query($query3);
                        $conn->query($query4);
                        $conn->query($query5);
                        $conn->query($query6);
                        if (!$conn->query($test_connexion)) {
                            $message = "Erreur d'accès aux serveurs xxxx, veuillez vérifier les informations rentrées.";
                            header("Location: ../admin/install.php?pb=$message");
                        } else {

                            setcookie("first_install", -1, time(), '/');
                            setcookie("install_finished", 1, time() + 365 * 30 * 24 * 60 * 60, '/');
                            unset($_COOKIE['first_install']);

                            $config = fopen('../include/config.inc.php', 'w+');
                            $contenu = '<?php 
                                        return array(
                                            "DB_HOST" => "' . $serveur . '",
                                            "DB_NAME" => "' . $nom . '",
                                            "DB_USER" => "' . $utilisateur . '",
                                            "DB_PASSWORD" => "' . $mdp . '",
                                            "DB_PORT" => ' . $port . ',
                                            "MAIL" => "' . $mail . '"
                                        );
                                    ?>';
                            if (fwrite($config, $contenu) === false) {
                                $message = "Erreur, veuillez réessayer.";
                                header("Location: ../admin/install.php?pb=$message");
                            } else {
                                fclose($config);
                                $message = 'Votre site est opérationnel, vous pouvez à présent l\'utiliser. Cette page s\'autodétruira dans 10 secondes.';
                                header("Location: ../admin/install.php?ok=$message");
                            }
                        }
                    }
                }
            }
        }
    }
}

//    "FAC_WR" => array(
//        "DB_HOST" => "10.40.128.23",
//        "DB_NAME" => "db2016l3idwa_wrozenbe",
//        "DB_USER" => "l3i_wrozenbe",
//        "DB_PASSWORD" => "A123456*"
//    ),
//    "FAC_AR" => array(
//        "DB_HOST" => "10.40.128.23",
//        "DB_NAME" => "db2016l3idwa_aremond",
//        "DB_USER" => "l3i_aremond",
//        "DB_PASSWORD" => "A123456*"
//    ),
//    "HOSTINGER" => array(
//        "DB_HOST" => "mysql.hostinger.fr",
//        "DB_NAME" => "u622820902_remro",
//        "DB_USER" => "u622820902_remro",
//        "DB_PASSWORD" => "CoMate9996Inc"
//    ),

