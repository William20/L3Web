<?php
///**
// * Created by PhpStorm.
// * User: William
// * Date: 11/10/2016
// * Time: 21:29
// */

require_once("../include/fonctions.inc.php");


// Redirige l'utilisateur s'il est déjà identifié
if (!empty($_COOKIE['utilisateur'])) {
    header("Location: ../content/accueil.php");
} else {
    if (isset($_POST['identifiant'])) {
        if (isset($_POST['email'])) {
            if (isset($_POST['mdp'])) {
                if (isset($_POST['mdp2'])) {
                    if (isset($_POST['submit'])) {

                        $conn = connexion_SQL();

                        $result = $conn->query("SELECT identifiant, email
                                        FROM Inscription
                                        WHERE identifiant = '" . $_POST["identifiant"] . "'
                                        OR email = '" . $_POST["email"] . "'");

                        // Vérification de l'unicité du nom d'utilisateur et de l'adresse e-mail
                        if ($result === FALSE) {
                            header("Location: ../content/inscription.php?insc=Ce nom d'utilisateur ou adresse email est déjà utilisé. Veuillez en choisir un autre#formulaire");
                        } else {
                            // Si un enregistrement est trouvé
                            if ($result->num_rows > 0) {
                                while ($row = $result->fetch_array()) {
                                    if ($_POST["identifiant"] == $row["identifiant"]) {
                                        $message = "Le nom d'utilisateur " . $_POST["identifiant"];
                                        $message .= " est déjà utilisé.";
                                    } elseif ($_POST["email"] == $row["email"]) {
                                        $message = "L'adresse e-mail " . $_POST["email"];
                                        $message .= " est déjà utilisée.";
                                    }
                                }
                                header("Location: ../content/inscription.php?insc=$message#formulaire");
                            } else {
                                // Génération de la clef d'activation
                                $caracteres = array("a", "b", "c", "d", "e", "f", 0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
                                $caracteres_aleatoires = array_rand($caracteres, 8);
                                $clef_activation = "";

                                foreach ($caracteres_aleatoires as $i) {
                                    $clef_activation .= $caracteres[$i];
                                }

                                // Création du compte utilisateur
                                $conn->query("
                              INSERT INTO Inscription(
                                   identifiant
                                   , email
                                   , mdp
                                   , cle_activation
                                   , compte_actif
                                   , voiture
                                   , place
                                   , prix
                                   , Dormir
                                   , placeD
                                   , prixD
                              )
                              VALUES(
                                   '" . $_POST["identifiant"] . "'
                                   , '" . $_POST["email"] . "'
                                   , '" . md5($_POST["mdp"]) . "'
                                   , '" . $clef_activation . "'
                                   , '0'
                                   , 'Non'
                                   , '0'
                                   , '0'
                                   , 'Non'
                                   , '0'
                                   , '0'
                              )");
                                $config = @require("../include/config.inc.php");
                                $mail = $config["MAIL"];
                                if ($mail == "OUI") {
                                    // Envoi du mail d'activation
                                    $sujet = "Activation de votre compte utilisateur";
                                    $msg = "Pour valider votre inscription, merci de cliquer sur ce lien :\n";
                                    $msg .= "http://comateincorporation.hol.es/traitement";
                                    $msg .= "/activer_compte.php?";
                                    $msg .= "clef=" . $clef_activation;
                                    $headers = 'From: CoMateIncorporation';

                                    mail($_POST['email'], $sujet, $msg, $headers);
                                    $message = "Vérifiez votre boîte mail pour activer votre compte !";
                                    header("Location: ../content/connexion.php?insc=$message");
                                } else {
                                    header("Location: ../content/activation_compte.php?clef_activation=$clef_activation");
                                }
                            }
                        }
                        $conn->close();
                    }
                }
            }
        }
    }
}