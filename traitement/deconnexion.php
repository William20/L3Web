<?php
/**
 * Created by PhpStorm.
 * User: amelie
 * Date: 15/10/16
 * Time: 13:40
 */

//on detruit le cookie de la session et on redirige vers la page de connection
if (isset($_COOKIE['utilisateur'])) {
    unset($_COOKIE['utilisateur']);
    setcookie('utilisateur', null, -1, '/');
    header('Location: ../content/connexion.php');
}
else {
    header('Location: ../content/accueil.php?');
}