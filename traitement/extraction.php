<?php
/**
 * Created by PhpStorm.
 * User: amelie
 * Date: 20/11/16
 * Time: 15:32
 */
require('../fpdf/fpdf.php');
require('../classes/PDF.php');

$pdf = new PDF('P', 'mm', 'A4');
$pdf->nom_de_evenement = $_POST['nom'];
$pdf->nom_du_document = "Résumé de l'événement " . $_POST['nom'];
$conn = connexion_SQL("../");

$result = $conn->query("SELECT date_evenement, lieu, description, participant, budget, voiture, dormir FROM Evenement WHERE nom = \"$pdf->nom_de_evenement\" ");
$row = $result->fetch_array();
$pdf->date = $row[0];
$pdf->lieu = $row[1];
$pdf->description = $row[2];
$pdf->participants = unserialize($row[3]);
$pdf->budget = $row[4];
$pdf->voiture = $row[5];
$pdf->dormir = $row[6];

if ($pdf->budget == "Oui") {
    $result = $conn->query("SELECT produit, prise, prix FROM Budget WHERE evenement = '$pdf->nom_de_evenement'");
    if ($result != false || $result->num_rows > 0) {
        $pdf->budget = $result->fetch_all();
    } else {
        $pdf->budget = array();
    }
}

if ($pdf->voiture == "Oui") {
    $voitureArray = array();
    foreach ($pdf->participants as $personne) {
        $result = $conn->query("SELECT voiture FROM Inscription WHERE identifiant LIKE BINARY '$personne' ");
        $resultVoiture = $result->fetch_array(MYSQLI_NUM)[0];
        if ($resultVoiture == "Oui") {
            $ligneVoiture = array();
            array_push($ligneVoiture, $personne);
            $infoVoiture = $conn->query("SELECT prix, place FROM Inscription WHERE identifiant LIKE BINARY '$personne' ");
            $info = $infoVoiture->fetch_array(MYSQLI_NUM);
            $result2 = $conn->query("SELECT passagers FROM Voiture WHERE (conducteur LIKE BINARY '" . $personne . "' AND evenement = '$pdf->nom_de_evenement' ) ");
            if ($result2 == true && $result2->num_rows > 0) {
                $array2 = unserialize($result2->fetch_array(MYSQLI_NUM)[0]);
            } else {
                $array2 = "";
            }
            $result3 = $conn->query("SELECT lieu FROM Voiture WHERE (conducteur LIKE BINARY '" . $personne . "' AND evenement = '$pdf->nom_de_evenement' )");
            if ($result3 == true && $result3->num_rows > 0) {
                $rdv1 = $result3->fetch_array(MYSQLI_NUM)[0];
            } else {
                $rdv1 = "";
            }
            array_push($ligneVoiture, $info[0], $info[1], $array2, $rdv1);
            array_push($voitureArray, $ligneVoiture);
        }
    }
    $pdf->voiture = $voitureArray;
}

if ($pdf->dormir == "Oui") {
    $dormirArray = array();
    foreach ($pdf->participants as $personne) {
        $result = $conn->query("SELECT Dormir FROM Inscription WHERE identifiant LIKE BINARY '$personne' ");
        $resultDormir = $result->fetch_array(MYSQLI_NUM)[0];
        if ($resultDormir == "Oui") {
            $ligneDormir = array();
            array_push($ligneDormir, $personne);
            $infoDormir = $conn->query("SELECT prixD FROM Inscription WHERE identifiant LIKE BINARY '$personne' )");
            if ($infoDormir == true && $infoDormir->num_rows > 0) {
                $info = $infoDormir->fetch_array(MYSQLI_NUM);
            } else {
                $info = array("");
            }
            $result2 = $conn->query("SELECT gens FROM Dormir WHERE ( hebergeur LIKE BINARY '" . $personne . "' AND evenement = '$pdf->nom_de_evenement' )");
            if ($result2 == true && $result2->num_rows > 0) {
                $array2 = unserialize($result2->fetch_array(MYSQLI_NUM)[0]);
            } else {
                $array2 = "";
            }
            array_push($ligneDormir, $info[0], $array2);
            array_push($dormirArray, $ligneDormir);
        }
    }
    $pdf->dormir = $dormirArray;
}

$conn->close();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->AjouterDescription("Description de l'événement :");
$pdf->AjouterParticipant("Les participants :");
$pdf->AjouterBudget("Organisation budgétaire :");
$pdf->AjouterCovoiturage("Organisation du covoiturage :");
$pdf->AjouterDormir("Organisation hébergement :");


//$pdf->Output();

$pdf->Output("D", $pdf->nom_du_document, true);
?>