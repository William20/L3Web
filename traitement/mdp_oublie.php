<?php
/**
 * Created by PhpStorm.
 * User: William
 * Date: 03/11/2016
 * Time: 20:58
 */

require_once("../include/fonctions.inc.php");

$conn = connexion_SQL();

$id = $_POST['id'];

$query = "SELECT email, identifiant
          FROM Inscription
          WHERE (identifiant = '$id' OR email = '$id')";

$test = $conn->query($query);
$donnees = $test->fetch_array(MYSQLI_NUM);

$email = $donnees[0];
$identifiant = $donnees[1];

if ($donnees != NULL) {

    $caracteres = array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", 0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
    $caracteres_aleatoires = array_rand($caracteres, 6);
    $faux_code = array_rand($caracteres, 8);

    $code_secret = "";
    $code_piege = "";

    foreach ($caracteres_aleatoires as $i) {
        $code_secret .= $caracteres[$i];
    }

    foreach ($faux_code as $i) {
        $code_piege .= $caracteres[$i];
    }

    $mail = @require_once("../include/config.inc.php");
    if ($mail["MAIL"] == "OUI") {

        $sujet = "Quelqu'un a demandé un nouveau mot de passe pour ";
        $sujet .= "votre compte CoMateIncorporation";

        $msg = "Bonjour $identifiant, quelqu'un a récemment demandé ";
        $msg .= "à réinitialiser votre mot de passe CoMateIncorporation.\n";
        $msg .= "Saisissez le code de réinitialisation du mot ";
        $msg .= "de passe : $code_secret si vous souhaitez bien le changer.";
        $msg .= "\nCoMateIncorporation";

        $headers = 'From: CoMateIncorporation';

        mail($email, $sujet, $msg, $headers);
        setcookie("code", $code_secret, time() + 60 * 60 * 24 * 30, '/');
        $md5code_secret = md5($code_secret);
        setcookie("mail_changer_mdp", $email, time() + 60 * 60 * 24 * 30, '/');
        header("Location: ../content/code_changer_mdp.php");
    } else {
        $message = 'Impossible de changer votre mot de passe ';
        $message .= 'car le site ne le permet pas, veuillez recréer un compte.';
        header("Location: ../content/inscription.php?insc=$message");
    }
}
else {
    $message = 'Impossible de changer votre mot de passe car le compte ';
    $message .= 'associé n\'existe pas';
    header("Location: ../content/inscription.php?insc=$message");
}