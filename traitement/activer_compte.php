<?php
/**
 * Created by PhpStorm.
 * User: william
 * Date: 16/10/16
 * Time: 13:16
 */

require_once ("../include/fonctions.inc.php");
// Redirige l'utilisateur s'il est déjà identifié
if (isset($_COOKIE["ID_UTILISATEUR"])) {
    header("Location: ../content/accueil.php");
} else {

// Vérifie que de bonnes valeurs sont passées en paramètres
    if (!preg_match("/^[a-f0-9]{8}$/i", strtolower($_GET["clef"]))) {
        header("Location: ../content/connexion.php");
    } else {

// Connexion à la base de données
        $conn = connexion_SQL();

// Sélection de l'utilisateur concerné
        $result = $conn->query("SELECT compte_actif
                                    , cle_activation
                                    FROM Inscription
                                    WHERE cle_activation = '" . strtolower($_GET["clef"]) . "'
                                    ");

// Si une erreur survient
        if (!$result) {
            $message = "Une erreur est survenue lors de l'activation de votre compte utilisateur";
        } else {

// Si aucun enregistrement n'est trouvé
            if ($result->num_rows == 0) {
                $message="Aucun enregistrement n'a été trouvé.";
                header("Location: ../content/inscription.php?insc=$message");
            } else {

// Récupération du tableau de données retourné
                $row = $result->fetch_array();

// Vérification que le compte ne soit pas déjà activé
                if ($row["compte_actif"] != 0) {
                    $message = "Votre compte utilisateur a déjà été activé";
                } else {
// Activation du compte utilisateur
                    $result = $conn->query("UPDATE Inscription
                                                SET compte_actif = '1'
                                                WHERE cle_activation = '" . strtolower($_GET["clef"]) . "'
                                                ");

// Si une erreur survient
                    if (!$result) {
                        $message = "Une erreur est survenue lors de l'activation de votre compte utilisateur";
                        header("Location: ../content/inscription.php?insc=$message");
                    } else {
                        $message = "Votre compte utilisateur a correctement été activé";
                    }
                }
            }
        }
        header("Location: ../content/connexion.php?pb=$message");
// Fermeture de la connexion à la base de données
        $conn->close();
    }
}