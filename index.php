<?php
/**
 * Created by PhpStorm.
 * User: wrozenberg
 * Date: 30/09/16
 * Time: 15:12
 */

/**
 * Index principal
 */
if (!empty($_COOKIE['utilisateur'])) {
    header("Location: content/accueil.php");
}
require_once("include/header.inc.php");
?>
    <div class="index">
        <header class="row">
            <div class="col-lg-12">
                CoMateIncorporation
            </div>
        </header>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2" style="font-size: 105%">
                CoMateIncorporation est un site de création et organisation d'événement pour les étudiants
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                <a href="index.php">
                    <img style="width:70%;" src="images/logo_CoMateIncorporation.png" alt="Logo CoMateIncorporation"/>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-lg-offset-3">
                <a class="liens_index" href="content/connexion.php">Connexion</a>
            </div>
            <div class="col-lg-2 col-lg-offset-2">
                <a class="liens_index" href="content/inscription.php">Inscription</a>
            </div>
        </div>
    </div>
<?php
require_once("include/footer.inc.php");
