<?php

/**
 * Created by PhpStorm.
 * User: wrozenberg
 * Date: 07/10/16
 * Time: 14:08
 */


$file = $_SERVER['PHP_SELF']; //Récupération du nom de la page dynamiquement
$filename = basename($file);
$path = "";

if ($filename != "index.php") {
    $path = "../";
}
require_once($path . "include/fonctions.inc.php");
$conn = connexion_SQL();

echo '<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="' . $path . 'bootstrap-3.3.7/dist/js/jquery.bootstrap-autohidingnavbar.js"></script>
<link rel="stylesheet" href="' . $path . 'bootstrap-3.3.7/dist/css/bootstrap.css">
<link rel="stylesheet" href="' . $path . 'style/style.css">
<link rel="icon" href="' . $path . 'images/logo_CoMateIncorporation.ico">
<script type="text/javascript" src="' . $path . 'js/script.js"></script>
<title>CoMateIncorporation</title>
</head>
<body>';
if ($filename != "index.php" && $filename != "connexion.php" && $filename != "inscription.php"
    && $filename != "install.php" && $filename != "mdp_oublie.php" && $filename != "changer_mdp.php"
    && $filename != "changer_mdp.php"
) { // Condidition à modifier pour rajouter des pages sans accès au menu
    ?>
    <script>
        //        if ($(window).width() < 992) {
        //            $(document).ready(function () {
        //                $(".navbar-fixed-top").autoHidingNavbar({
        //                    'showOnBottom': false,
        //                    'showOnUpscroll': false
        //                });
        //            });
        //        };
        $(function () {
            var pgurl = window.location.href.substr(window.location.href
                    .lastIndexOf("/") + 1);
            $("li a").each(function () {
                var href = this.getAttribute('href').substr(this.getAttribute('href').lastIndexOf("/") + 1);
                if (href == pgurl || href == '')
                    $(this).parent('li').addClass("active");
            });
        });
    </script>
    <?php echo '
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <ul class="nav navbar-nav">
                <li class="link"><a href="../content/accueil.php">Accueil</a></li>
                <li class="link"><a href="../content/creation.php">Créer un événement</a></li>
                <li class="link"><a href="../content/evenements.php">Je participe</a></li>
                <li class="link"><a href="../content/liste.php">J\'organise</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="../traitement/deconnexion.php"><span class="glyphicon glyphicon-log-out"></span> Se déconnecter</a></li>
                <li ><a href = "../content/profil.php"><span class="glyphicon glyphicon-user" ></span> Mon profil</a></li>
            </ul >
        </div >
    </nav >
    ';
}
echo '<div class="container">';
?>
