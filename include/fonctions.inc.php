<?php
/**
 * Created by PhpStorm.
 * User: wrozenberg
 * Date: 10/10/16
 * Time: 17:38
 */

$file = $_SERVER['PHP_SELF']; //Récupération du nom de la page dynamiquement, ainsi que du sous-$nom
$filename = basename($file);
if ($filename != "index.php") {
    define('path', "../");
} else define('path', "");

function existDB($db)
{


}

function connexion_SQL($path = path)
{
    $dbname = "DB_NAME";
    if (file_exists($path . "include/config.inc.php")) {
        $connexion = @require_once($path . "include/config.inc.php");

        $servername = "DB_HOST";
        $username = "DB_USER";
        $password = "DB_PASSWORD";
        $port = "DB_PORT";

        if ($port == "") {
            $port = 3306;
        }

        $conn = new mysqli($connexion[$servername], $connexion[$username],
            $connexion[$password], "", $connexion[$port]);
        if($conn->select_db($connexion[$dbname])) {
        }
        else echo "La base de donnée demandée n'existe pas ou a été supprimée.";
    } else
        $conn = "";

    return $conn;
}

function logo() //Fonction qui place le logo selon l'emplacement désigné par le paramètre
{
    echo '
        <div class="row" style="text - align: center">
            <a href="' . path . 'index . php">
                <img style="width:30 %;" src="' . path . 'images / logo_CoMateIncorporation . png" 
                    alt="Logo CoMateIncorporation"/>
            </a>
        </div>';
}

function bdd($event)
{
    $nom = $event->getNom();
    $description = $event->getDescription();
    $participant = $event->getParticipant();
    $date = $event->getDate();
    $admin = $event->getAdmin();
    $visibilite = $event->getVisibilite();
    $budget = $event->getBudget();
    $voiture = $event->getVoiture();
    $dormir = $event->getDormir();
    $lieu = $event->getLieu();

    $conn = connexion_SQL();

    $arrayParticipant = serialize($participant);

    //Variable qui contient du script SQL
    $result = $conn->query("SELECT * FROM Evenement WHERE nom = \"$nom\" ");
    // Vérification de l'unicité du nom
    if ($result == TRUE) {
        // Si un enregistrement est trouvé
        if ($conn->query("
                              INSERT INTO Evenement(
                                   nom
                                   , lieu
                                   , description
                                   , date_evenement
                                   , participant
                                   , admin
                                   , visibilite
                                   , budget
                                   , voiture
                                   , Dormir
                                   , compteur
                              )
                              VALUES(
                                   \"$nom\"
                                   ,\"$lieu\"
                                   ,\"$description\"
                                   , \"$date\"
                                   , '" . $arrayParticipant . "'
                                   , '" . $admin . "'
                                   ,\"$visibilite\"
                                    ,\"$budget\"
                                    ,\"$voiture\"
                                    ,\"$dormir\"
                                    ,'0'
                                                                 )") === FALSE
        ) {
            $message = "Erreur d'accès lors de la création de l'événement, veuillez recommencer ou contacter un administrateur . ";
        }
        header("Location: ../content/afficher_evenement.php?nom=" . $nom);
    } else {
    }
// Fermeture de la connexion à la base de données
    $conn->close();
}

function afficher_event($conn)
{
    $donnees = array();
    $result = $conn->query("SELECT nom, lieu, description, date_evenement, visibilite, compteur FROM Evenement WHERE visibilite ='Publique' ");
    if ($result == TRUE && ($result->num_rows > 0)) {
        while ($ligne = $result->fetch_array(MYSQLI_NUM)) {
            array_push($donnees, $ligne);
        }

        foreach ($donnees as $event) {
            ?>
            <div class="col-lg-10 col-lg-offset-1 jumbotron"
                 style="font-size: 90%;background-color: #f6f6de;padding: 10px 100px 10px 100px; margin-top: 20px">
                <h2 style="font-size: 300%"><u><?php echo $event[0]; ?></u></h2>
                <p><?php echo "<b>Lieu :</b> " . $event[1]; ?></p>
                <p><?php echo "<b>Date :</b> " . $event[3]; ?></p>
                <p><?php echo "<b>Description :</b> " . $event[2]; ?></p>
                <?php if ($event[5] != '0') { ?>
                    <p><?php echo "<b>Cet événement a été vu :</b> " . $event[5] . " fois"; ?></p>
                    <?php
                }
                ?>
                <form method="post" action="../content/afficher_evenement.php">
                    <input type="hidden" name="Nom_Event_acc" value="<?php echo $event[0]; ?>"/>
                    <input type="submit" value="Afficher les détails de l'événement"/>
                </form>
                <?php
                $parti = $conn->query("SELECT participant FROM Evenement WHERE nom = \"$event[0]\" ");
                $ligneParti = $parti->fetch_array(MYSQLI_NUM);
                $participant = unserialize($ligneParti[0]);
                $flag = 1;
                foreach ($participant as $personne) {
                    if ($personne == $_COOKIE['utilisateur']) {
                        $flag = 0;
                    }
                }
                if ($flag != 1) {
                    echo '
                        <div class="row alert alert-success" style="margin-top: 10px">
                            <strong>Vous participez à cet événement</strong> 
                        </div>';
                } ?>
            </div>
            <?php
        }
    } else { ?>
        <div class="jumbotron">
            <h2>Aucun événement disponible</h2>
        </div>
        <?php
    }
    $conn->close();
}

function afficher_eventAdmin($conn)
{
    $donnees = array();
    $result = $conn->query("SELECT nom, lieu, description, date_evenement, visibilite, admin FROM Evenement");
    if ($result == TRUE && ($result->num_rows > 0)) {
        while ($ligne = $result->fetch_array(MYSQLI_NUM)) {
            array_push($donnees, $ligne);
        }
        $tmp = 0;
        foreach ($donnees as $event) {
            $admin = $event[5];
            if ($admin == $_COOKIE['utilisateur']) {
                $tmp = 1;
                ?>
                <div class="col-lg-10 col-lg-offset-1 jumbotron"
                     style="font-size: 90%;background-color: #f6f6de;padding: 10px 100px 10px 100px; margin-top: 20px">
                    <h2 style="font-size: 300%"><u><?php echo $event[0]; ?></u></h2>
                    <p><?php echo "<b>Lieu :</b> " . $event[1]; ?></p>
                    <p><?php echo "<b>Date :</b> " . $event[3]; ?></p>
                    <p><?php echo "<b>Description :</b> " . $event[2]; ?></p>
                    <form method="post" action="../content/gerer_evenement.php">
                        <input type="hidden" name="Nom_Event" value="<?php echo $event[0]; ?>"/>
                        <input type="submit" value="Modifier l'événement"/>
                    </form>
                    <?php
                    $parti = $conn->query("SELECT participant FROM Evenement WHERE nom = \"$event[0] \" ");
                    $ligneParti = $parti->fetch_array(MYSQLI_NUM);
                    $participant = unserialize($ligneParti[0]);
                    $flag = 1;
                    foreach ($participant as $personne) {
                        if ($personne == $_COOKIE['utilisateur']) {
                            $flag = 0;
                        }
                    }
                    if ($flag != 1) {
                        echo "<div class=\"row alert alert-success\" style=\"margin-top: 10px\">
                            <strong>Vous participez à cet événement</strong> 
                        </div>";
                    } ?>
                </div>
                <?php
            }
        }
        if ($tmp != 1) {
            ?>
            <div class="jumbotron">
                <h2>Aucun événement disponible</h2>
            </div>
            <?php
        }
    } else { ?>
        <div class="jumbotron">
            <h2>Aucun événement disponible</h2>
        </div>
        <?php
    }
    ?>
    <?php
    $conn->close();
}

function afficher_eventParti($conn)
{
    $i = 0;
    $donnees = array();
    $donneesInfo = array();
    $result = $conn->query("SELECT participant FROM Evenement");
    $info = $conn->query("SELECT nom, lieu, description, date_evenement, visibilite FROM Evenement");
    $tmpp = 0;

    if ($result == TRUE && ($result->num_rows > 0) && $info == TRUE) {
        while ($ligne = $result->fetch_array(MYSQLI_NUM)) {
            $tmp = unserialize($ligne[0]);
            array_push($donnees, $tmp);
        }
        while ($ligneInfo = $info->fetch_array(MYSQLI_NUM)) {
            array_push($donneesInfo, $ligneInfo);
        }
        foreach ($donnees as $event) {
            if ($event != array()) {
                if (appartient($event, $_COOKIE['utilisateur']) == 0) {
                    $tmpp = 1;
                    $eventInfo = $donneesInfo[$i];
                    $i++;
                    ?>
                    <div class="col-lg-10 col-lg-offset-1 jumbotron"
                         style="font-size: 90%;background-color: #f6f6de;padding: 10px 100px 10px 100px; margin-top: 20px">
                        <h2 style="font-size: 300%"><u><?php echo $eventInfo[0]; ?></u></h2>
                        <p><?php echo "<b>Lieu :</b> " . $eventInfo[1]; ?></p>
                        <p><?php echo "<b>Date :</b> " . $eventInfo[3]; ?></p>
                        <p><?php echo "<b>Description :</b> " . $eventInfo[2]; ?></p>
                        <form method="post" action="../content/afficher_evenement.php">
                            <input type="hidden" name="Nom_Event_acc" value="<?php echo $eventInfo[0]; ?>"/>
                            <input type="submit" value="Afficher les détails de l'événement"/>
                        </form>
                    </div>
                    <?php
                } else {
                    $i++;
                }
            } else {
               $tmpp=1;
            }
        }
        if ($tmpp != 1) {
            ?>
            <div class="jumbotron">
                <h2>Vous ne participez à aucun événement.</h2>
            </div>
            <?php
        }
    } else {
        ?>
        <div class="jumbotron">
            <h2>Vous ne participez à aucun événement.</h2>
        </div>
        <?php
        return;
    }
    ?>
    <?php
    $conn->close();
}

function appartient($array, $utilisateur)
{
    foreach ($array as $a) {
        if ($a == $utilisateur) {
            return 0;
        }
    }
    return 1;

}


function efface_cookies()
{
    foreach ($_COOKIE as $key => $cok) {
        setcookie($key, -1, time(), '/');
        unset($_COOKIE["$key"]);
    }
}