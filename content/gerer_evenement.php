<?php
/**
 * Created by PhpStorm.
 * User: amelie
 * Date: 13/10/16
 * Time: 14:32
 */

if (!isset($_COOKIE['utilisateur'])) {
    header('Location: ../content/connexion.php');
}
if (isset($_POST["Nom_Event"])) {
    $valeurNom = $_POST["Nom_Event"];
} else if (isset($_COOKIE["modif"])) {
    $valeurNom = $_COOKIE["modif"];
}
setcookie('nom_e', $valeurNom, time() + 60 * 60 * 24 * 30, '/');

require_once("../include/header.inc.php");

$info = $conn->query("SELECT lieu, description, date_evenement, visibilite, budget, voiture, Dormir FROM Evenement WHERE nom =\"$valeurNom\" ");
$result = $conn->query("SELECT participant  FROM Evenement WHERE nom =\"$valeurNom\" ");
$desc = $info->fetch_array(MYSQLI_NUM);
$participant = unserialize($result->fetch_array(MYSQLI_NUM)[0]);

?>

<header id="myHeader"><?php echo $valeurNom; ?></header>

<?php
if (isset($_GET["pb"])) {
    if ($_GET["pb"] == 4) {
        echo '<div class="row alert alert-success" style="margin-top: 10px">Modifications réussies</div>';
    }
}
?>

<div class="jumbotron" id="gerer">
    <form method="post" action="../traitement/modificationEvent.php">
        <div class="form-group">
            <label for="titre_id">
                Nom de l'événement :
                <input class="form-control" type="text" name="titre"
                       value="<?php echo $valeurNom ?>" id="titre_id"
                       maxlength="40" required/>
            </label>
        </div>
        <div class="form-group">
            <label for="descrption_id">
                Description de l'événement :
                <textarea class="form-control" name="desc" id="descrption_id"
                          maxlength="500" rows="5" cols="40" required><?php echo $desc[1]; ?>
                </textarea>
            </label>
        </div>
        <div class="form-group">
            <label for="lieu_id">
                Lieu de l'événement :
                <input class="form-control" type="text" name="lieu"
                       value="<?php echo $desc[0]; ?>" maxlength="500"
                       id="lieu_id" required>
            </label>
        </div>
        <div class="form-group">
            <div id="date_update">
                <label for="date_id">
                    Date de l'événement :
                    <input class="form-control" onchange="dateUpdate();" type="date" name="date" id="date_id"
                           value='<?php echo $desc[2] ?>' required/>
                </label>
            </div>
        </div>
        <div class="form-group" id="invit">
            <label>
                Visibilité de l'événement :
                <select class="form-control" name="visi" id="visi_id" size="1">
                    <?php
                    if ($desc[3] == "Publique") {
                        ?>
                        <option selected="selected">
                            Publique
                        </option>
                        <option>
                            Privé
                        </option>
                        <?php
                    } else {
                        ?>
                        <option>
                            Publique
                        </option>
                        <option selected="selected">
                            Privé
                        </option>
                        <?php
                    }
                    ?>
                </select>
            </label>
        </div>

        <div class="form-group" id="Liste_Participants">
            <label>
                Liste des participants :
            </label>
            <ul id="participants">
                <?php
                foreach ($participant as $personne) {
                    ?>
                    <li id="li_<?php echo $personne; ?>">
                        <?php
                        echo $personne;
                        if ($personne != $_COOKIE['utilisateur']) {
                            ?>
                            <input onclick="suppParticipant(this);" class='btn-circle btn-danger btn' type='button'
                                   id="<?php echo $personne; ?>" value='X'/>
                            <?php
                        }
                        ?>
                    </li>
                <?php } ?>
            </ul>
            <button onclick="addChampParticipant();" class='btn btn-default' type='button'
                    value="Ajouter quelqu'un d'autre">
                Ajouter quelqu'un d'autre
            </button>
        </div>

        <?php
        if ($desc[4] == "Oui") {
            ?>
            <div class="form-group" id=budgetDiv>
                <label for="budget">Gestion du budget :</label>
                <table id="budget">
                    <tr>
                        <th class="tdthBudget">Produit</th>
                        <th class="tdthBudget">Prise en charge</th>
                        <th class="tdthBudget">Coût (en euros)</th>
                    </tr>
                    <?php
                    $resultB = $conn->query("SELECT * FROM `Budget` WHERE evenement = \"$valeurNom\"");
                    if ($resultB == true && $resultB->num_rows > 0) {
                        $ifor = 1;
                        $pas = $resultB->num_rows;
                    } else {
                        $pas = 1;
                        $ifor = 0;
                        $valueB = array("", "", "");
                        $placeholder1 = "Bouteille d'eau";
                        $placeholder2 = "Jean";
                        $placeholder3 = "2";
                    }
                    for ($i = 1; $i <= $pas; $i++) {
                        if ($ifor) {
                            $value = $conn->query("SELECT produit, prise, prix FROM Budget WHERE evenement = \"$valeurNom\" ");
                            $valueB = $value->fetch_all();
                            $placeholder1 = "";
                            $placeholder2 = "";
                            $placeholder3 = "";
                        }
                        ?>
                        <tr id="<?php echo $i; ?>">
                            <td>
                                <input onblur="addObjt(this);"
                                       value="<?php if ($valueB != array("", "", "")) echo $valueB[$i - 1][0]; ?>"
                                       type="text" id="<?php echo $i; ?>"
                                       name="objet<?php echo $i; ?>"
                                       placeholder="<?php echo $placeholder1; ?>" required/>
                            </td>
                            <td>
                                <input onblur="addPrise(this);"
                                       value="<?php if ($valueB != array("", "", "")) echo $valueB[$i - 1][1]; ?>"
                                       type="text" id="<?php echo $i; ?>"
                                       name="prise<?php echo $i; ?>"
                                       placeholder="<?php echo $placeholder2; ?>"/>
                            </td>
                            <td>
                                <input onblur="addPrix(this);"
                                       value="<?php if ($valueB != array("", "", "")) echo $valueB[$i - 1][2]; ?>"
                                       type="number" id="<?php echo $i; ?>"
                                       name="prix<?php echo $i; ?>"
                                       placeholder="<?php echo $placeholder1; ?>"/>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
                <input id='+' onclick='addLigneB(<?php echo $pas ?>);' type='button' value='+'/>
                <input id='fonction_a_changer' onclick='suppLigneB(<?php echo $resultB->num_rows ?>);' type='button'
                       value='-'/>
                <br>
                <input class="btn btn-default" onclick="SuppB();" type="button" name="suppB" id="suppB"
                       value="Supprimer l'option "/>
            </div>
            <?php
        } else { ?>
            <div class="form-group" id="budgetDiv">
                <label for="budget">Gestion du budget :</label>
                <input class="btn btn-default" onclick="AddB();"
                       type="button" name="suppB" id="suppB" value="Activer l'option"/>
            </div>
            <?php
        }
        if ($desc[5] == "Oui") {
            ?>
            <div class="form-group" id="voitureDiv">
                <label for="budget">Gestion du covoiturage :</label>
                <?php
                $savoir_siVoiture = 0;
                foreach ($participant as $personne) {
                    $result = $conn->query("SELECT voiture FROM Inscription WHERE identifiant LIKE BINARY '$personne' ");
                    $resultVoiture = $result->fetch_array(MYSQLI_NUM)[0];
                    if ($resultVoiture == "Oui") {
                        $savoir_siVoiture = 1;
                        ?>
                        <h4>Passagers dans voiture de <?php echo $personne; ?></h4>
                        <h5>Cliquez sur une case pour vous inscrire ou vous désinscrire (vous ne pouvez pas vous
                            inscrire
                            dans votre
                            propre
                            voiture mais vous pouvez supprimer des utilisateurs de votre tableau)</h5>
                        <?php
                        $resultprixPlace = $conn->query("SELECT prix, place FROM Inscription WHERE identifiant LIKE BINARY '$personne' ");
                        $prixPlace = $resultprixPlace->fetch_array(MYSQLI_NUM);
                        $prix = $prixPlace[0];
                        $place = $prixPlace[1];
                        $result2 = $conn->query("SELECT passagers, lieuRdv FROM Voiture WHERE (conducteur LIKE BINARY '$personne' AND evenement = \"$valeurNom\") ");
                        $passagersRdv = $result2->fetch_row();
                        $array2 = unserialize($passagersRdv[0]);
                        $lieuRDV = $passagersRdv[1];
                        echo '<h2 style="margin-top: 2%; font-size: 120%; font-weight: bold"> Le prix par personne et par trajet moyen (environ 15min de temps de trajet) est de ' . $prix . ' euros pour cette voiture';
                        echo '<h2 style="margin-top: 2%; font-size: 120%; font-weight: bold"> Le lieu de rendez-vous est : ' . $lieuRDV;
                        $table = "Tablevoiture";
                        if ($personne == $_COOKIE['utilisateur'])
                            $table = "Tablevoiture hoverNon";
                        echo '<table class="' . $table . '">';
                        if (intval(($place / 2)) == (($place - 1) / 2)) {
                            for ($i = 1; $i < ($place) - 1;) {
                                $j = $i + 1;
                                $id1 = $personne . "V_" . $i;
                                $id2 = $personne . "V_" . $j;
                                if ($personne == $_COOKIE['utilisateur']) {
                                    $onclick = "virerVoiture('" . $personne . "',this.id);";
                                    $onclick2 = $onclick;
                                } else {
                                    $onclick = "ajoutVoiture('" . $personne . "V_" . $i . "','" . $_COOKIE["utilisateur"] . "','" . $personne . "','" . $place . "','" . $prix . "')";
                                    $onclick2 = "ajoutVoiture('" . $personne . "V_" . $j . "','" . $_COOKIE["utilisateur"] . "','" . $personne . "','" . $place . "','" . $prix . "')";

                                }
                                $j = $i + 1;
                                ?>
                                <tr id="<?php echo $personne; ?>">
                                    <td class="tdthvoiture" id='<?php echo $id1; ?>'
                                        onclick="<? echo $onclick; ?>"><?php if (isset($array2[$i - 1])) echo $array2[$i - 1] ?></td>
                                    <td class="tdthvoiture" id='<?php echo $id2; ?>'
                                        onclick="<? echo $onclick; ?>"><?php if (isset($array2[$i])) echo $array2[$i] ?></td>
                                </tr>
                                <?php
                                $i = $i + 2;
                            }
                            $onclick3 = "ajoutVoiture('" . $personne . "V_" . $place . "','" . $_COOKIE["utilisateur"] . "','" . $personne . "','" . $place . "','" . $prix . "')";
                            ?>

                            <tr id="<?php echo $personne; ?>">
                                <td class="tdthvoiture" colspan="2" id='<?php echo $personne . 'V_' . $place; ?>'
                                    onclick="<? echo $onclick3; ?>"><?php if (isset($array2[$place - 1])) echo $array2[$place - 1] ?></td>
                            </tr>
                            <?php
                        } else {
                            for ($i = 1; $i < $place;) {
                                $j = $i + 1;
                                $id1 = $personne . "V_" . $i;
                                $id2 = $personne . "V_" . $j;
                                if ($personne == $_COOKIE['utilisateur']) {
                                    $onclick = "virerVoiture('" . $personne . "',this.id);";
                                    $onclick2 = $onclick;
                                } else {
                                    $onclick = "ajoutVoiture('" . $personne . "V_" . $i . "','" . $_COOKIE["utilisateur"] . "','" . $personne . "','" . $place . "','" . $prix . "')";
                                    $onclick2 = "ajoutVoiture('" . $personne . "V_" . $j . "','" . $_COOKIE["utilisateur"] . "','" . $personne . "','" . $place . "','" . $prix . "')";

                                }
                                ?>
                                <tr id="<?php echo $personne; ?>">
                                    <td class="tdthvoiture" id='<?php echo $id1; ?>'
                                        onclick="<? echo $onclick; ?>"><?php if (isset($array2[$i - 1])) echo $array2[$i - 1] ?></td>
                                    <td class="tdthvoiture" id='<?php echo $id2; ?>'
                                        onclick="<? echo $onclick2; ?>"><?php if (isset($array2[$i])) echo $array2[$i] ?></td>
                                </tr>

                                <?php
                                $i = $i + 2;
                            }
                        }
                        echo '</table>';
                        if ($personne == $_COOKIE['utilisateur']) {
                            ?>
                            <input type="text" name="NomUti" hidden value="<? echo $personne; ?>">
                            <label for="lieu_rdv_id"> Donnez un lieu de rendez-vous :</label>
                            <textarea class="form-control"
                                      placeholder="Exemple : RDV a 18h au 2 avenue Adolphe Chauvin"
                                      name="rdv" id="lieu_rdv_id" maxlength="300"
                                      rows="3"
                                      cols="30"
                                      required><?php if (isset($lieuRDV)) echo $lieuRDV; ?></textarea>
                            <?php
                        }
                    }
                }
                if ($savoir_siVoiture == 0) {
                    echo "<p>Aucun participant n'a la possibilité d'utiliser une voiture</p>";
                }
                ?>
                <input class="btn-default btn" onclick="SuppV();" type="button" name="suppV" id="suppV"
                       value="Supprimer l'option "/>
            </div>
            <?php
        } else {
            echo '<div class="form-group" id=voitureDiv>';
            echo '<label>Gestion du covoiturage :</label>';
            echo '<input class="btn btn-default" onclick="AddV();" value="Activer l\'option" id="activer" type="button" style="margin-top: 3%" >';
            echo '</div>';
        }
        if ($desc[6] == "Oui") {
            echo '<div class="form-group" id=dormirDiv>';
            ?>
            <label for="budget">Gestion de l'hebergement :</label>
            <?php
            $savoir_siDormir = 0;
            foreach ($participant as $personne) {
                $result = $conn->query("SELECT Dormir FROM Inscription WHERE identifiant LIKE BINARY '$personne' ");
                $resultDormir = $result->fetch_array(MYSQLI_NUM)[0];
                if ($resultDormir == "Oui") {
                    $savoir_siDormir = 1;
                    ?>
                    <h4>Qui dort chez <?php echo $personne; ?> ?</h4>
                    <h5>Cliquez sur une case pour vous inscrire ou vous désinscrire (vous ne pouvez pas vous
                        inscrire
                        dans votre
                        propre
                        voiture mais vous pouvez supprimer des utilisateurs de votre tableau)</h5>
                    <?php
                    $resultprixPlace = $conn->query("SELECT prixD, placeD FROM Inscription WHERE identifiant LIKE BINARY '$personne' ");
                    $prixPlace = $resultprixPlace->fetch_array(MYSQLI_NUM);
                    $prix = $prixPlace[0];
                    $place = $prixPlace[1];
                    $result2 = $conn->query("SELECT gens FROM Dormir WHERE (hebergeur LIKE BINARY '$personne' AND evenement = \"$valeurNom\") ");
                    $passagersRdv = $result2->fetch_row();
                    $array2 = unserialize($passagersRdv[0]);
                    echo '<h2 style="margin-top: 2%; font-size: 120%; font-weight: bold"> Le prix par personne et par nuit est de  ' . $prix . ' euros pour cet hébergeur';
                    $table = "Tablevoiture";
                    if ($personne == $_COOKIE['utilisateur'])
                        $table = "Tablevoiture hoverNon";
                    echo '<table class="' . $table . '">';
                    if (intval(($place / 2)) == (($place - 1) / 2)) {
                        for ($i = 1; $i < ($place) - 1;) {
                            $j = $i + 1;
                            $id1 = $personne . "D_" . $i;
                            $id2 = $personne . "D_" . $j;
                            if ($personne == $_COOKIE['utilisateur']) {
                                $onclick = "virerDormir('" . $personne . "',this.id);";
                                $onclick2 = $onclick;
                            } else {
                                $onclick = "ajoutDormir('" . $personne . "D_" . $i . "','" . $_COOKIE["utilisateur"] . "','" . $personne . "','" . $place . "','" . $prix . "')";
                                $onclick2 = "ajoutDormir('" . $personne . "D_" . $j . "','" . $_COOKIE["utilisateur"] . "','" . $personne . "','" . $place . "','" . $prix . "')";

                            }
                            $j = $i + 1;
                            ?>
                            <tr id="<?php echo $personne; ?>">
                                <td class="tdthvoiture" id='<?php echo $id1; ?>'
                                    onclick="<? echo $onclick; ?>"><?php if (isset($array2[$i - 1])) echo $array2[$i - 1] ?></td>
                                <td class="tdthvoiture" id='<?php echo $id2; ?>'
                                    onclick="<? echo $onclick; ?>"><?php if (isset($array2[$i])) echo $array2[$i] ?></td>
                            </tr>
                            <?php
                            $i = $i + 2;
                        }
                        $onclick3 = "ajoutDormir('" . $personne . "D_" . $place . "','" . $_COOKIE["utilisateur"] . "','" . $personne . "','" . $place . "','" . $prix . "')";
                        ?>

                        <tr id="<?php echo $personne; ?>">
                            <td class="tdthDoiture" colspan="2" id='<?php echo $personne . 'D_' . $place; ?>'
                                onclick="<? echo $onclick3; ?>"><?php if (isset($array2[$place - 1])) echo $array2[$place - 1] ?></td>
                        </tr>
                        <?php
                    } else {
                        for ($i = 1; $i < $place;) {
                            $j = $i + 1;
                            $id1 = $personne . "D_" . $i;
                            $id2 = $personne . "D_" . $j;
                            if ($personne == $_COOKIE['utilisateur']) {
                                $onclick = "virerDormir('" . $personne . "',this.id);";
                                $onclick2 = $onclick;
                            } else {
                                $onclick = "ajoutDormir('" . $personne . "D_" . $i . "','" . $_COOKIE["utilisateur"] . "','" . $personne . "','" . $place . "','" . $prix . "')";
                                $onclick2 = "ajoutDormir('" . $personne . "D_" . $j . "','" . $_COOKIE["utilisateur"] . "','" . $personne . "','" . $place . "','" . $prix . "')";

                            }
                            ?>
                            <tr id="<?php echo $personne; ?>">
                                <td class="tdthvoiture" id='<?php echo $id1; ?>'
                                    onclick="<? echo $onclick; ?>"><?php if (isset($array2[$i - 1])) echo $array2[$i - 1] ?></td>
                                <td class="tdthvoiture" id='<?php echo $id2; ?>'
                                    onclick="<? echo $onclick2; ?>"><?php if (isset($array2[$i])) echo $array2[$i] ?></td>
                            </tr>

                            <?php
                            $i = $i + 2;
                        }
                    }
                    echo '</table>';
                }
            }
            if ($savoir_siDormir == 0) {
                echo "<p>Aucun participant n'a la possibilité d'héberger</p>";
            }
            echo '<input class="btn btn-default" onclick="SuppD();" type="button" name="suppD" id="suppD"
                       value="Supprimer l\'option "/>';
            echo '</div>';
        } else {
            echo '<div class="form-group" id=dormirDiv>';
            echo '<label>Qui dort où ?</label>';
            echo '<input class="btn btn-default" onclick="AddD();" value="Activer l\'option" id="activer" type="button" style="margin-top: 3%" >';
            echo '</div>';
        }
        ?>
        <p><input type="submit" id="ok" value="Sauvegarder les modifications"/></p>
    </form>
</div>

<form onsubmit="return confirm('Êtes-vous sûr de vouloir supprimer cet événement ?');" method="post"
      action="../traitement/suppEvent.php">
    <p><input type="submit" id="ok" value="Supprimer l'événement"/></p>
</form>
<a href="gerer_evenement.php#">
    <img style="width=5vmmax; height: 5vmax; margin-left: 80%" src="../images/Haut.png">
</a>
<?php
require_once("../include/footer.inc.php");
?>
