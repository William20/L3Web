<?php
/**
 * Created by PhpStorm.
 * User: william
 * Date: 04/11/16
 * Time: 15:47
 */

if (!isset($_COOKIE['utilisateur'])) {
    header('Location: ../content/accueil.php');
}
require_once("../include/header.inc.php");
?>
    <form action="../traitement/changer_mdp.php" onsubmit="return verifForm(this);" method="post">
        <div class="form-group">
            <label for="password">
                Nouveau mot de passe :
                <input class="form-control"
                       oninput="verifMdp(this);memeMdp(document.getElementById('password_confirm'), this);"
                       type="password"
                       id="password" name="mdp" placeholder="Nouveau mot de passe"/>
            </label>
        </div>
        <div class="form-group">
            <label for="password_confirm">
                Ressaisir le mot de passe :
                <input class="form-control" oninput="memeMdp(this, document.getElementById('password'));"
                       type="password"
                       id="password_confirm"
                       name="mdp2" placeholder="Ressaisir le mot de passe"/>
            </label>
        </div>
        <button class="btn btn-default" type="submit" name="submit" value="OK">OK</button>
    </form>
<?php
require_once("../include/footer.inc.php");