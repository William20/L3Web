<?php
/**
 * Created by PhpStorm.
 * User: william
 * Date: 04/11/16
 * Time: 14:13
 */

if (!isset($_COOKIE['utilisateur'])) {
    header('Location: ../content/accueil.php');
}
require_once("../include/header.inc.php");
$mdp = $_GET['mde'];
?>
    <form action="../content/changer_mdp.php"
          onsubmit="return verifCode(document.getElementById('code_sec').value, <?php echo $_COOKIE['code']; ?>)"
          method="post">
        <div class="form-group">
            <label for="code_sec">
                Code confidentiel :
                <input class="form-control" type="text" id="code_sec" size="7" name="code" placeholder="######" aria-label="######"/>
            </label>
        </div>
        <input class="btn-default btn" type="submit" name="submit" value="OK"/>
    </form>

<?php
require_once("../include/footer.inc.php");