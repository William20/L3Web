<?php
/**
 * Created by PhpStorm.
 * User: amelie
 * Date: 13/10/16
 * Time: 11:49
 */
if (!isset($_COOKIE['utilisateur'])) {
    header('Location: ../content/connexion.php');
}
require_once("../include/header.inc.php");
?>
    <header xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">Créer un événement</header>
<?php
if (isset($_GET['creat'])) {
    echo "<p>" . $_GET['creat'] . "</p>";
}
?>
    <form class="jumbotron" style="margin: 30px;" method="post" action="../traitement/creer_evenement.php">
        <div class="form-group" id="NameEvent">
            <label for="titre_id">Nom de votre événement :</label>
            <input onblur="verifEvenement(this, this.value);" class="form-control" type="text" name="titre" value="" id="titre_id"
                   maxlength="40" placeholder="Exemple : mon événement" required/>
        </div>
        <div class="form-group">
            <label for="lieu_id">Lieu :</label>
            <input class="form-control" type="text" name="lieu" value="" maxlength="500" id="lieu_id"
                   placeholder="Exemple : Cergy" required>
        </div>
        <div class="form-group">
            <label for="descrption_id"> Décrivez votre projet en quelques lignes :</label>
            <textarea class="form-control"
                      placeholder="Exemple : soirée entre étudiants de l'université de Cergy-Pontoise le vendredi soir après les cours"
                      name="desc" id="descrption_id" value="" maxlength="500" rows="5" cols="40" required></textarea>
        </div>
        <div class="form-group" id="invit">
            <label for="visi_id">Visibilité de l'événement :</label>
            <select class="form-control" onchange="invitation();" name="visi" id="visi_id" size="1">
                <option>Publique</option>
                <option>Privé</option>
            </select>
        </div>
        <div class="form-group" id="date_update">
            <label for="date_id"> Date de l'événement :</label>
            <input class="form-control" onchange="dateUpdate();" type="date" name="date" id="date_id"
                   value='<?php echo date("Y"); ?>-<?php echo date("m"); ?>-<?php echo date("d"); ?>' required/>
        </div>

        <div>
            <label>Voulez-vous activer l'option budget ?</label>
            <a data-toggle="collapse" href="#budget_button">En savoir plus</a>
            <div id="budget_button" class="collapse">
                Si vous prévoyez un événement nécéssitant une organisation
                budgétaire, cette option vous sera utile !
            </div>

            <label class="radio-inline"><input type="radio" name="budget" value="Oui" id="budget_Oid"
                                               checked/>Oui</label>
            <label class="radio-inline"><input type="radio" name="budget" value="Non" id="budget_Nid"/>Non</label>
        </div>

        <div>
            <label for="voiture">Voulez-vous activer l'option covoiturage ?</label>
            <a href="#covoiturage_a" data-toggle="collapse">En savoir plus</a>
            <div id="covoiturage_a" class="collapse">
                Si vous prévoyez un événement nécéssitant une organisation
                de covoiturage, cette option vous sera utile
                !
            </div>
            <label class="radio-inline"><input type="radio" name="voiture" value="Oui" id="voiture_Oid"
                                               checked/>Oui</label>
            <label class="radio-inline"><input type="radio" name="voiture" value="Non" id="voiture_Nid" L>Non</label>
        </div>

        <div>
            <label for="dormir">Voulez-vous activer l'option qui dort où ?</label>
            <a data-toggle="collapse" href="#dormir_a">En savoir plus</a>
            <div id="dormir_a" class="collapse">
                Si vous prévoyez un événement nécéssitant une organisation
                de type hebergement, cette option vous sera utile !
            </div>
            <label class="radio-inline"><input type="radio" name="dormir" value="Oui" id="dormir_Oid"
                                               checked>Oui</label>
            <label class="radio-inline"><input type="radio" name="dormir" value="Non" id="dormir_Nid">Non</label>
        </div>
        <button class="btn btn-default" style="margin-top: 20px" type="submit" id="ok" value="Créer">Créer</button>
    </form>
<?php
require_once("../include/footer.inc.php");


