<?php
/**
 * Created by PhpStorm.
 * User: amelie
 * Date: 13/10/16
 * Time: 08:48
 */
if (!isset($_COOKIE['utilisateur'])) {
    header('Location: ../content/connexion.php');
}
require_once("../include/header.inc.php");
?>
<header>Événements que vous organisez</header>
<?php afficher_eventAdmin($conn); ?>
<?php
require_once("../include/footer.inc.php");
?>