<?php
/**
 * Created by PhpStorm.
 * User: William
 * Date: 03/11/2016
 * Time: 20:51
 */

require_once("../include/header.inc.php");
?>
    <div class="jumbotron">
        <h1>Mot de passe oublié</h1>
        <form action="../traitement/mdp_oublie.php" method="post">
            <div class="form-group">
                <label for="id">
                    <input style="width: 150%;" class="form-control" type="text" id="id" name="id"
                           placeholder="Entrez votre pseudo ou votre adresse mail"/>
                </label>
            </div>
            <div class="form-group">
                <label for="id">
                    <input class="form-control" type="submit" id="submit" value="OK"/>
                </label>
            </div>
        </form>
    </div>
<?php
require_once("../include/footer.inc.php");