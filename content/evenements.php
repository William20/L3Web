<?php
/**
 * Created by PhpStorm.
 * User: amelie
 * Date: 13/10/16
 * Time: 08:48
 */
if (!isset($_COOKIE['utilisateur'])) {
    header('Location: ../content/connexion.php');
}
require_once("../include/header.inc.php");
?>

    <header>Vos participations</header>
    <div class="row">
        <?php afficher_eventParti($conn); ?>
    </div>
<?php
require_once("../include/footer.inc.php");