<?php
/**
 * Created by PhpStorm.
 * User: amelie
 * Date: 13/10/16
 * Time: 14:32
 */
if (!isset($_COOKIE['utilisateur'])) {
    header('Location: ../content/connexion.php');
}
$nom = "";
if (isset($_POST["Nom_Event"])) {
    $nom = $_POST['Nom_Event'];
} else if (isset($_GET['nom'])) {
    $nom = $_GET['nom'];
} else if (isset($_POST["Nom_Event_acc"])) {
    $nom = $_POST['Nom_Event_acc'];
}

if (isset($_POST["Nom_Event"])) {
    $nom_evenement = $_POST["Nom_Event"];
    $valeurNom = $_POST["Nom_Event"];
} else if (isset($_GET['nom'])) {
    $nom_evenement = $_GET["nom"];
    $valeurNom = $_GET["nom"];
} else if (isset($_POST["Nom_Event_acc"])) {
    $nom_evenement = $_POST["Nom_Event_acc"];
    $valeurNom = $_POST["Nom_Event_acc"];
}

$newNom = str_replace(" ", "/", $valeurNom);

setcookie('nom_e', $newNom, time() + 60 * 60 * 24 * 30, '/');

if (!isset($_COOKIE[$newNom])) {
    setcookie($newNom, '1', time() + 60 * 60 * 24 * 30, '/');
}
require_once("../include/header.inc.php");
$info = $conn->query("SELECT lieu, description, date_evenement, budget, voiture, Dormir FROM Evenement WHERE nom =\"$nom\" ");
$result = $conn->query("SELECT participant FROM Evenement WHERE nom =\"$nom\" ");
$resultAdmin = $conn->query("SELECT admin FROM Evenement WHERE nom =\"$nom\" ");
$desc = $info->fetch_array(MYSQLI_NUM);
$participant = unserialize($result->fetch_array(MYSQLI_NUM)[0]);
$administrateur = $resultAdmin->fetch_array(MYSQLI_NUM)[0];

if (!isset($_COOKIE[$newNom])) {
    $result = $conn->query("SELECT compteur FROM Evenement WHERE nom =\"$valeurNom\" ");
    $compteur = $result->fetch_array(MYSQLI_NUM)[0];
    $newcompteur = $compteur + 1;
    $conn->query("UPDATE Evenement SET compteur='$newcompteur' WHERE nom =\"$valeurNom\" ");
}
?>
    <header id="myHeader"><?php echo $nom_evenement; ?></header>
    <div class="jumbotron" id="gerer">
<?php
$c = 1;
if (appartient($participant, $_COOKIE['utilisateur']) == 0) {
    $c = 0;
    if ($desc[2] < date("Y") . '-' . date("m") . '-' . date("d")) {
        $disabled = "disabled";
        $msg = "";
    }
    if ($administrateur == $_COOKIE['utilisateur']) {
        $disabled = "disabled";
        $msg = "En tant qu'administrateur vous ne pouvez pas vous désinscrire.";
    } else {
        $msg = "";
        $disabled = "";
    }
    ?>
    <form method="post" action="../traitement/desinscription_evenement.php">
        <input type="submit" id="inscription" <?php echo $disabled; ?> value="Se désinscrire"/>
    </form>
    <p style="color: red;"><?php echo $msg; ?></p>
    <?php

} else {
    $c = 1;
}
if ($c == 1) {
    if ($desc[2] < date("Y") . '-' . date("m") . '-' . date("d")) {
        $disabled = "disabled";
        $msg = "Cet événement est terminé vous ne pouvez pas vous inscrire.";
    } else {
        $disabled = "";
        $msg = "";
    } ?>
    <form method="post" action="../traitement/inscription_evenement.php">
        <input type="submit" id="inscription" <?php echo $disabled; ?> value="S'inscrire"/>
    </form>
    <p style="color: red;"><?php echo $msg; ?></p>
    <?php
}
?>
    <div class="panel panel-body">
        <label> Description de l'événement :</label>
        <?php
        echo "<p>" . $desc[1] . "</p>";
        ?>
    </div>
    <div class="panel panel-body">
        <label> Lieu de l'événement :</label>
        <?php
        echo "<p>" . $desc[0] . "</p>";
        ?>
    </div>

    <div class="panel panel-body">
        <label> Date de l'événement :</label>
        <?php
        echo "<p>" . $desc[2] . "</p>"; ?>
    </div>

    <div class="panel panel-body">
        <label>Administrateur de l'événement :</label>
        <ul>
            <?php
            $adminM = $conn->query("SELECT email FROM Inscription WHERE identifiant ='" . $administrateur . "' ");
            $email = $adminM->fetch_array(MYSQLI_NUM)[0];
            echo "<li>" . $administrateur . " : " . $email . "</li>";
            ?>
        </ul>
    </div>
    <div class="panel panel-body">
        <label> Liste des participants :</label>
        <ul>
            <?php
            foreach ($participant as $personne) {
                echo "<li>" . $personne . "</li>";
            }
            ?>
        </ul>
    </div>
<?php
if (appartient($participant, $_COOKIE['utilisateur']) == 0) {
    if ($desc[3] == "Oui") {
        ?>
        <div class="panel panel-body">
            <div class="form-group" id=budgetDiv>
                <label for="budget">Gestion du budget :</label>
                <table id="budget">
                    <tr>
                        <th class="tdthBudget">Produit</th>
                        <th class="tdthBudget">Prise en charge</th>
                        <th class="tdthBudget">Coût (en euros)</th>
                    </tr>
                    <?php
                    $resultB = $conn->query("SELECT * FROM `Budget` WHERE evenement = \"$nom_evenement\"");
                    if ($resultB == true && $resultB->num_rows > 0) {
                        $ifor = 1;
                        $pas = $resultB->num_rows;
                    } else {
                        $pas = 1;
                        $ifor = 0;
                        $valueB = array("", "", "");
                        $placeholder1 = "Bouteille d'eau";
                        $placeholder2 = "Jean";
                        $placeholder3 = "2";
                    }
                    for ($i = 1; $i <= $pas; $i++) {
                        if ($ifor) {
                            $value = $conn->query("SELECT produit, prise, prix FROM Budget WHERE evenement = \"$nom_evenement\" ");
                            $valueB = $value->fetch_all();
                            $placeholder1 = "";
                            $placeholder2 = "";
                            $placeholder3 = "";
                        }
                        ?>
                        <tr id="<?php echo $i; ?>">
                            <td>
                                <input onblur="addObjt(this);"
                                       value="<?php echo $valueB[$i-1][0]; ?>"
                                       type="text" id="<?php echo $i; ?>"
                                       name="objet<?php echo $i; ?>"
                                       placeholder="<?php echo $placeholder1; ?>" required/>
                            </td>
                            <td>
                                <input onblur="addPrise(this);"
                                       value="<?php echo $valueB[$i-1][1]; ?>"
                                       type="text" id="<?php echo $i; ?>"
                                       name="prise<?php echo $i; ?>"
                                       placeholder="<?php echo $placeholder2; ?>"/>
                            </td>
                            <td>
                                <input onblur="addPrix(this);"
                                       value="<?php echo $valueB[$i-1][2]; ?>"
                                       type="number" id="<?php echo $i; ?>"
                                       name="prix<?php echo $i; ?>"
                                       placeholder="<?php echo $placeholder1; ?>"/>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
                <input id='+' onclick='addLigneB(<?php echo $pas ?>);' type='button' value='+'/>
                <input id='fonction_a_changer' onclick='suppLigneB(<?php echo $resultB->num_rows ?>);' type='button'
                       value='-'/>
                <br>
            </div>
        </div>
        <?php
    }
    if ($desc[4] == "Oui") {
        ?>
        <div class="panel panel-body">
            <div class="form-group" id="voitureDiv">
                <label for="budget">Gestion du covoiturage :</label>
                <?php
                $savoir_siVoiture = 0;
                foreach ($participant as $personne) {
                    $result = $conn->query("SELECT voiture FROM Inscription WHERE identifiant LIKE BINARY '$personne' ");
                    $resultVoiture = $result->fetch_array(MYSQLI_NUM)[0];
                    if ($resultVoiture == "Oui") {
                        $savoir_siVoiture = 1;
                        ?>
                        <h4>Passagers dans voiture de <?php echo $personne; ?></h4>
                        <h5>Cliquez sur une case pour vous inscrire ou vous désinscrire (vous ne pouvez pas vous
                            inscrire
                            dans votre
                            propre
                            voiture mais vous pouvez supprimer des utilisateurs de votre tableau)</h5>
                        <?php
                        $resultprixPlace = $conn->query("SELECT prix, place FROM Inscription WHERE identifiant LIKE BINARY '$personne' ");
                        $prixPlace = $resultprixPlace->fetch_array(MYSQLI_NUM);
                        $prix = $prixPlace[0];
                        $place = $prixPlace[1];
                        $result2 = $conn->query("SELECT passagers, lieuRdv FROM Voiture WHERE (conducteur LIKE BINARY '$personne' AND evenement = \"$nom_evenement\") ");
                        $passagersRdv = $result2->fetch_row();
                        $array2 = unserialize($passagersRdv[0]);
                        $lieuRDV = $passagersRdv[1];
                        echo '<h2 style="margin-top: 2%; font-size: 120%; font-weight: bold"> Le prix par personne et par trajet moyen (environ 15min de temps de trajet) est de ' . $prix . ' euros pour cette voiture';
                        echo '<h2 style="margin-top: 2%; font-size: 120%; font-weight: bold"> Le lieu de rendez-vous est : ' . $lieuRDV;
                        $table = "Tablevoiture";
                        if ($personne == $_COOKIE['utilisateur'])
                            $table = "Tablevoiture hoverNon";
                        echo '<table class="'.$table.'">';
                        if (intval(($place / 2)) == (($place - 1) / 2)) {
                            for ($i = 1; $i < ($place) - 1;) {
                                $j = $i + 1;
                                $id1=$personne."V_".$i;
                                $id2=$personne."V_".$j;
                                if ($personne == $_COOKIE['utilisateur']) {
                                    $onclick = "virerVoiture('" . $personne . "',this.id);";
                                    $onclick2=$onclick;
                                } else {
                                    $onclick = "ajoutVoiture('".$personne."V_".$i."','".$_COOKIE["utilisateur"]."','".$personne."','".$place."','".$prix."')";
                                    $onclick2 = "ajoutVoiture('".$personne."V_".$j."','".$_COOKIE["utilisateur"]."','".$personne."','".$place."','".$prix."')";

                                }
                                $j = $i + 1;
                                ?>
                                <tr id="<?php echo $personne; ?>">
                                    <td class="tdthvoiture" id='<?php echo $id1; ?>'
                                        onclick="<? echo $onclick; ?>"><?php if (isset($array2[$i - 1])) echo $array2[$i - 1] ?></td>
                                    <td class="tdthvoiture" id='<?php echo $id2; ?>'
                                        onclick="<? echo $onclick; ?>"><?php if (isset($array2[$i])) echo $array2[$i] ?></td>
                                </tr>
                                <?php
                                $i = $i + 2;
                            }
                            $onclick3 = "ajoutVoiture('".$personne."V_".$place."','".$_COOKIE["utilisateur"]."','".$personne."','".$place."','".$prix."')";
                            ?>

                            <tr id="<?php echo $personne; ?>">
                                <td class="tdthvoiture" colspan="2" id='<?php echo $personne . 'V_' . $place; ?>'
                                    onclick="<? echo $onclick3; ?>"><?php if (isset($array2[$place - 1])) echo $array2[$place - 1] ?></td>
                            </tr>
                            <?php
                        } else {
                            for ($i = 1; $i < $place;) {
                                $j = $i + 1;
                                $id1=$personne."V_".$i;
                                $id2=$personne."V_".$j;
                                if ($personne == $_COOKIE['utilisateur']) {
                                    $onclick = "virerVoiture('" . $personne . "',this.id);";
                                    $onclick2=$onclick;
                                } else {
                                    $onclick = "ajoutVoiture('".$personne."V_".$i."','".$_COOKIE["utilisateur"]."','".$personne."','".$place."','".$prix."')";
                                    $onclick2 = "ajoutVoiture('".$personne."V_".$j."','".$_COOKIE["utilisateur"]."','".$personne."','".$place."','".$prix."')";

                                }
                                ?>
                                <tr id="<?php echo $personne; ?>">
                                    <td class="tdthvoiture" id='<?php echo $id1; ?>'
                                        onclick="<? echo $onclick; ?>"><?php if (isset($array2[$i - 1])) echo $array2[$i - 1] ?></td>
                                    <td class="tdthvoiture" id='<?php echo $id2; ?>'
                                        onclick="<? echo $onclick2; ?>"><?php if (isset($array2[$i])) echo $array2[$i] ?></td>
                                </tr>

                                <?php
                                $i = $i + 2;
                            }
                        }
                        echo '</table>';
                        if ($personne == $_COOKIE['utilisateur']) {
                            ?>
                            <form method="post" action="../traitement/modificationRDV.php">
                                <input type="text" name="NomUti" hidden value="<? echo $personne; ?>">
                                <label for="lieu_rdv_id"> Donnez un lieu de rendez-vous :</label>
                                <textarea class="form-control"
                                          placeholder="Exemple : RDV a 18h au 2 avenue Adolphe Chauvin"
                                          name="rdv" id="lieu_rdv_id" maxlength="300"
                                          rows="3"
                                          cols="30"
                                          required><?php if (isset($lieuRDV)) echo $lieuRDV; ?></textarea>
                                <input type="submit" value="Modifier mon lieu de rdv">
                            </form>
                            <?php
                        }
                    }
                }
                if ($savoir_siVoiture == 0) {
                    echo "<p>Aucun participant n'a la possibilité d'utiliser une voiture</p>";
                }
                ?>
            </div>
        </div>
        <?php
    }
    if ($desc[5] == "Oui") {
        ?>
        <div class="panel panel-body">
            <div class="form-group" id="dormirDiv">
                <label for="budget">Gestion de l'hebergement :</label>
                <?php
                $savoir_siDormir = 0;
                foreach ($participant as $personne) {
                    $result = $conn->query("SELECT Dormir FROM Inscription WHERE identifiant LIKE BINARY '$personne' ");
                    $resultDormir = $result->fetch_array(MYSQLI_NUM)[0];
                    if ($resultDormir == "Oui") {
                        $savoir_siDormir = 1;
                        ?>
                        <h4>Qui dort chez <?php echo $personne; ?> ?</h4>
                        <h5>Cliquez sur une case pour vous inscrire ou vous désinscrire (vous ne pouvez pas vous
                            inscrire
                            dans votre
                            propre
                            voiture mais vous pouvez supprimer des utilisateurs de votre tableau)</h5>
                        <?php
                        $resultprixPlace = $conn->query("SELECT prixD, placeD FROM Inscription WHERE identifiant LIKE BINARY '$personne' ");
                        $prixPlace = $resultprixPlace->fetch_array(MYSQLI_NUM);
                        $prix = $prixPlace[0];
                        $place = $prixPlace[1];
                        $result2 = $conn->query("SELECT gens FROM Dormir WHERE (hebergeur LIKE BINARY '$personne' AND evenement = \"$nom_evenement\") ");
                        $passagersRdv = $result2->fetch_row();
                        $array2 = unserialize($passagersRdv[0]);
                        echo '<h2 style="margin-top: 2%; font-size: 120%; font-weight: bold"> Le prix par personne et par nuit est de  ' . $prix . ' euros pour cet hébergeur';
                        $table = "Tablevoiture";
                        if ($personne == $_COOKIE['utilisateur'])
                            $table = "Tablevoiture hoverNon";
                        echo '<table class="' . $table . '">';
                        if (intval(($place / 2)) == (($place - 1) / 2)) {
                            for ($i = 1; $i < ($place) - 1;) {
                                $j = $i + 1;
                                $id1 = $personne . "D_" . $i;
                                $id2 = $personne . "D_" . $j;
                                if ($personne == $_COOKIE['utilisateur']) {
                                    $onclick = "virerDormir('" . $personne . "',this.id);";
                                    $onclick2 = $onclick;
                                } else {
                                    $onclick = "ajoutDormir('" . $personne . "D_" . $i . "','" . $_COOKIE["utilisateur"] . "','" . $personne . "','" . $place . "','" . $prix . "')";
                                    $onclick2 = "ajoutDormir('" . $personne . "D_" . $j . "','" . $_COOKIE["utilisateur"] . "','" . $personne . "','" . $place . "','" . $prix . "')";

                                }
                                $j = $i + 1;
                                ?>
                                <tr id="<?php echo $personne; ?>">
                                    <td class="tdthvoiture" id='<?php echo $id1; ?>'
                                        onclick="<? echo $onclick; ?>"><?php if (isset($array2[$i - 1])) echo $array2[$i - 1] ?></td>
                                    <td class="tdthvoiture" id='<?php echo $id2; ?>'
                                        onclick="<? echo $onclick; ?>"><?php if (isset($array2[$i])) echo $array2[$i] ?></td>
                                </tr>
                                <?php
                                $i = $i + 2;
                            }
                            $onclick3 = "ajoutDormir('" . $personne . "D_" . $place . "','" . $_COOKIE["utilisateur"] . "','" . $personne . "','" . $place . "','" . $prix . "')";
                            ?>

                            <tr id="<?php echo $personne; ?>">
                                <td class="tdthvoiture" colspan="2" id='<?php echo $personne . 'D_' . $place; ?>'
                                    onclick="<?php echo $onclick3; ?>"><?php if (isset($array2[$place - 1])) echo $array2[$place - 1];?></td>
                            </tr>
                            <?php
                        } else {
                            for ($i = 1; $i < $place;) {
                                $j = $i + 1;
                                $id1 = $personne . "D_" . $i;
                                $id2 = $personne . "D_" . $j;
                                if ($personne == $_COOKIE['utilisateur']) {
                                    $onclick = "virerDormir('" . $personne . "',this.id);";
                                    $onclick2 = $onclick;
                                } else {
                                    $onclick = "ajoutDormir('" . $personne . "D_" . $i . "','" . $_COOKIE["utilisateur"] . "','" . $personne . "','" . $place . "','" . $prix . "')";
                                    $onclick2 = "ajoutDormir('" . $personne . "D_" . $j . "','" . $_COOKIE["utilisateur"] . "','" . $personne . "','" . $place . "','" . $prix . "')";

                                }
                                ?>
                                <tr id="<?php echo $personne; ?>">
                                    <td class="tdthvoiture" id='<?php echo $id1; ?>'
                                        onclick="<? echo $onclick; ?>"><?php if (isset($array2[$i - 1])) echo $array2[$i - 1] ?></td>
                                    <td class="tdthvoiture" id='<?php echo $id2; ?>'
                                        onclick="<? echo $onclick2; ?>"><?php if (isset($array2[$i])) echo $array2[$i] ?></td>
                                </tr>

                                <?php
                                $i = $i + 2;
                            }
                        }
                        echo '</table>';
                    }
                }
                if ($savoir_siDormir == 0) {
                    echo "<p>Aucun participant n'a la possibilité d'héberger</p>";
                }
                ?>
            </div>
        </div>
        <?php
    }
    ?>
    <form method="post" action="../traitement/extraction.php">
        <input type="text" name="nom" value="<?php echo $nom_evenement ?>" hidden>
        <input type="submit" id="extraction" value="Télécharger un résumé au format PDF"/>
    </form>
    </div>
    <?php
}
require_once("../include/footer.inc.php");
?>