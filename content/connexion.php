<?php
/**
 * Created by PhpStorm.
 * User: wrozenberg
 * Date: 10/10/16
 * Time: 16:51
 */
if (isset($_COOKIE['utilisateur'])) {
    header('Location: ../content/accueil.php');
}
require_once("../include/header.inc.php");
?>
<div class="row">
    <a href="../index.php"><img style="width:20%;" src="../images/logo_CoMateIncorporation.png"
                                alt="Logo CoMateIncorporation"/></a>
</div>

<header class="row connexion">
    <div class="col-lg-4 col-lg-offset-4">
        Connexion
    </div>
</header>
<?php
if (isset($_GET["insc"])) {
    echo '<p>' . $_GET["insc"] . '</p>';
}
if (isset($_GET['pb'])) {
    echo '<p>' . $_GET['pb'] . '</p>';
}

if (isset($_GET['mdp_change'])) {
    echo '<p>' . $_GET['mdp_change'] . '</p>';
}
?>
<form class="conn" method="post" action="../traitement/connexion.php">
    <div class="form-group">
        <label for="identifiant">Identifiant :</label>
        <input class="form-control" type="text" id="identifiant" name="identifiant" placeholder="Identifiant"/>
    </div>
    <div class="form-group">
        <label for="mdp">Mot de passe :</label>
        <input class="form-control" type="password" id="mdp" name="mdp" placeholder="Mot de passe"/>
    </div>
    <div class="checkbox">
        <label><input name="souvenir" type="checkbox"> Se souvenir de moi</label>
    </div>
    <button type="submit" class="btn btn-default"> Ok</button>
</form>
<div class="row">
    <div class="col-lg-2 col-lg-offset-5">
        <a href="mdp_oublie.php">Mot de passe oublié ?</a>
    </div>
</div>
<div class="row">
    <div style="margin-bottom: 1%;" class="col-lg-2 col-lg-offset-5">
        <a href="inscription.php">Créer un compte</a>
    </div>
</div>
<?php
require_once("../include/footer.inc.php");
?>
