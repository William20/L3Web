<?php
/**
 * Created by PhpStorm.
 * User: amelie
 * Date: 13/10/16
 * Time: 14:32
 */
if (!isset($_COOKIE['utilisateur'])) {
    header('Location: ../content/connexion.php');
}
require_once("../include/header.inc.php");
$identifiant = $_COOKIE["utilisateur"];
$dormir = "SELECT dormir FROM Inscription WHERE identifiant LIKE BINARY '$identifiant' ";
$result = $conn->query($dormir);
$resultDormir = $result->fetch_array(MYSQLI_NUM)[0];
if ($resultDormir == "Non") {
    $checked_oui_dormir = "";
    $checked_non_dormir = "checked";
    $OuiNonDormir = "";
} else {
    $checked_oui_dormir = "checked";
    $checked_non_dormir = "";
    $prixDRequest = "SELECT prixD FROM Inscription WHERE identifiant LIKE BINARY '$identifiant' ";
    $resultPrixD = $conn->query($prixDRequest);
    $prixD = $resultPrixD->fetch_array(MYSQLI_NUM)[0];
    $placeDRequest = "SELECT placeD FROM Inscription WHERE identifiant LIKE BINARY '$identifiant' ";
    $resultPlaceD = $conn->query($placeDRequest);
    $placeD = $resultPlaceD->fetch_array(MYSQLI_NUM)[0];
    $OuiNonDormir = '
        <div class="form-group" id="div_add_idD">
            <label for="placeD">
                Combien de personnes pouvez-vous héberger (sans vous compter) :
                <input class="form-control" id="placeD" name="placeD"
                       value="' . $placeD . '" type="number" required/>
            </label>
            <br />
            <label for="prixD">
                Fixez un prix (en euros) par personne pour une nuit :
                <input class="form-control" id="prixD" name="prixD"
                       value="' . $prixD . '" type="number" required/>
            </label>
        </div>';
}
$voiture = "SELECT voiture FROM Inscription WHERE identifiant LIKE BINARY '$identifiant' ";
$result = $conn->query($voiture);
$resultVoiture = $result->fetch_array(MYSQLI_NUM)[0];
if ($resultVoiture == "Non") {
    $checked_oui_voiture = "";
    $checked_non_voiture = "checked";
    $OuiNonVoiture = "";
} else {
    $checked_oui_voiture = "checked";
    $checked_non_voiture = "";
    $prixRequest = "SELECT prix FROM Inscription WHERE identifiant LIKE BINARY '$identifiant' ";
    $resultPrix = $conn->query($prixRequest);
    $prix = $resultPrix->fetch_array(MYSQLI_NUM)[0];
    $placeRequest = "SELECT place FROM Inscription WHERE identifiant LIKE BINARY '$identifiant' ";
    $resultPlace = $conn->query($placeRequest);
    $place = $resultPlace->fetch_array(MYSQLI_NUM)[0];
    $OuiNonVoiture = '
        <div class="form-group" id="div_add_id">
            <label for="place">
                Nombre de personnes que vous pouvez transporter (sans vous compter) :
                <input class="form-control" id="place" name="place"
                   value="' . $place . '"
                   type="number" required/>
            </label>
            <label for="prix">
                Fixez un prix (en euros) par personne pour un trajet moyen
                (environ 15 minutes de temps de trajet) :
                <input class="form-control" id="prix" name="prix"
                   value="' . $prix . '"
                   type="number" required/>
            </label>
        </div>';
}
?>
    <header>
        Mes informations
    </header>
    <h1 id='js'><?php echo $identifiant; ?></h1>
<?php
if (isset($_GET["pb"])) {
    if ($_GET["pb"] == 4) {
        echo '<div class="row alert alert-success" style="margin-top: 10px">Modifications réussies</div>';
    }
}
?>
    <div class="jumbotron">
        <div id="modifUtilisateur">
            <form method="post" onsubmit="return verifMdp(document.getElementById('modifier_mdp'));" action="../traitement/modification.php">
                <div class="panel panel-body form-group">
                    <label for="modifier_mdp">Modifier mon mot de passe :
                        <input oninput="verifMdp(this)" class="form-control" type="text" id="modifier_mdp"
                               value="<?php echo $_COOKIE["mdp"]; ?>"
                               name='mdp' required/>
                    </label>
                </div>
                <div class="panel panel-body form-group" id="Voiture">
                    <label>Avez-vous le permis et une voiture disponible ?</label>
                    <div class="radio">
                        <label>
                            <input onclick="parametreVoiture(document.getElementById('div_add_id'));" type="radio"
                                   name="voiture"
                                   value="Oui" id="voiture_Oid" <?php echo $checked_oui_voiture; ?>/>
                            Oui
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input onclick="retirerPar();" type="radio" name="voiture"
                                   value="Non" id="voiture_Nid" <?php echo $checked_non_voiture; ?>/>
                            Non
                        </label>
                    </div>
                    <?php echo $OuiNonVoiture; ?>
                </div>
                <div class="panel panel-body form-group" id="Dormir">
                    <label>Pouvez-vous héberger des personnes chez vous ?</label>
                    <div>
                        <label class="radio-inline">
                            <input onclick="parametreDormir();" type="radio" name="dormir"
                                   value="Oui"
                                   id="dormir_Oid" <?php echo $checked_oui_dormir; ?>/>
                            Oui
                        </label>
                        <label class="radio-inline">
                            <input onclick="retirerParD();" type="radio" name="dormir"
                                   value="Non"
                                   id="dormir_Nid" <?php echo $checked_non_dormir; ?>/>
                            Non
                        </label>
                    </div>
                    <?php echo $OuiNonDormir; ?>
                </div>
                <button type="submit" class="btn-info btn" id='ok' value="Modifier mes informations">
                    Modifier mes informations
                </button>
            </form>
        </div>
        <form onsubmit="return confirm('Êtes-vous sûr de vouloir supprimer votre compte ?');" method="post"
              action="../traitement/suppCompte.php">
            <button type="submit" class="btn-danger btn" id="ok" value="Je souhaite supprimer mon compte">
                Je souhaite supprimer mon compte
            </button>
        </form>
    </div>
<?php
require_once("../include/footer.inc.php");