<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Activation compte</title>
</head>
<body>
<p>L'activation de compte par mail est indisponible.</p>
<p>
    Pour activer votre compte, merci de cliquer sur ce
    <a href="../traitement/activer_compte.php?clef=<?php echo $_GET['clef_activation']; ?>">lien.</a>
</p>
</body>
</html>