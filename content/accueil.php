<?php
/**
 * Created by PhpStorm.
 * User: amelie
 * Date: 13/10/16
 * Time: 08:48
 */
if (!isset($_COOKIE['utilisateur'])) {
    header('Location: ../content/connexion.php');
}
require_once("../include/header.inc.php");
;?>
<header>Accueil</header>
<div class="row">
    <form style="margin-top: 5px" class="col-lg-6 col-lg-offset-3" method="post"
          action="../traitement/afficher_recherche.php">
        <input type="text" id="recherche" name="recherche" placeholder="Rechercher un événement..." size="50"/>
        <script>
            $(function () {
                $('#recherche').autocomplete({
                    source: '../traitement/recherche.php'
                });
            });
        </script>
        <input type="submit" value="OK"/>
    </form>
</div>
<div class="row">
    <p><?php $var = isset($_GET['error']) ? $_GET['error'] : "";
        echo $var; ?></p>
    <?php if (isset($_GET["recherche"])) {
        echo '<p>' . $_GET["recherche"] . '</p>';
    } else {
        afficher_event($conn);
    } ?>
</div>
<?php
require_once("../include/footer.inc.php");
?>
