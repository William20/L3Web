<?php
/**
 * Created by PhpStorm.
 * User: wrozenberg
 * Date: 10/10/16
 * Time: 17:12
 */

if (isset($_COOKIE['utilisateur'])) {
    header('Location: ../content/accueil.php');
}
require_once("../include/header.inc.php");
?>
<div class="row" xmlns="http://www.w3.org/1999/html">
    <a href="../index.php"><img style="width:15%;" src="../images/logo_CoMateIncorporation.png"
                                alt="Logo CoMateIncorporation"/></a>
</div>
<header class="row inscription">
    <div class="col-lg-12">
        Inscription
    </div>
</header>
<?php
if (isset($_GET['insc'])) {
    echo "<div class=\"row alert alert-danger\">" . $_GET['insc'] . "</div>";
}
?>
<form class="insc" id="formulaire" method="post" action="../traitement/inscription.php"
      onsubmit="return verifForm(this)">
    <div class="form-group">
        <label for="identifiant">Identifiant :</label>
        <input class="form-control" oninput="verifPseudo(this);" id="identifiant" type="text" placeholder="Identifiant"
               name="identifiant"/>
    </div>
    <div class="form-group">
        <label for="email">Adresse mail :</label>
        <input class="form-control" oninput="verifMail(this);" id="email" type="email" placeholder="Adresse mail"
               name="email"/>
    </div>
    <div class="form-group">
        <label for="password">Mot de passe :</label>
        <input class="form-control"
               oninput="verifMdp(this); memeMdp(document.getElementById('password_confirm'), this);"
               id="password"
               type="password" placeholder="Mot de passe" name="mdp"/>
    </div>
    <div class="form-group">
        <label for="password_confirm">Ressaisir le mot de passe :</label>
        <input class="form-control" oninput="memeMdp(this, document.getElementById('password'));" id="password_confirm"
               type="password"
               placeholder="Ressaisir le mot de passe" name="mdp2"/>
    </div>
    <button class="btn btn-default" name="submit" type="submit">OK</button>
</form>
<div class="row connexion_insc">
    <div class="col-lg-4 col-lg-offset-4">
        <a href="connexion.php">Se connecter</a>
    </div>
</div>
<?php
require_once("../include/footer.inc.php");
?>

