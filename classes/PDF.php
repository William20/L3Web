<?php

/**
 * Created by PhpStorm.
 * User: amelie
 * Date: 20/11/16
 * Time: 16:51
 */

require_once("../include/fonctions.inc.php");

class PDF extends FPDF
{
    public $nom_du_document;
    public $nom_de_evenement;
    public $date;
    public $lieu;
    public $description;
    public $participants;
    public $budget;
    public $voiture;
    public $dormir;

    function Footer()
    {
        $this->SetY(-15);
        $this->SetFont('Arial', 'I', 8);
        $this->SetTextColor(128);
        $this->Cell(0, 10, 'Page ' . $this->PageNo(), 0, 0, 'C');
    }

    function Header()
    {
        $this->Image('../images/logo_CoMateIncorporation.png', 10, 6, 30);
        $findelargeur = ($this->w) - (40);
        $this->Image('../images/ucp.png', $findelargeur, 6, 30);
        $this->SetFont('Arial', 'B', 20);
        $mid_x = $this->w / 2;
        $this->Text($mid_x - ($this->GetStringWidth(utf8_decode($this->nom_du_document)) / 2), 30, utf8_decode($this->nom_du_document));
        $this->Ln(40);
    }

    function AjouterDescription($titre)
    {
        $this->TitreChapitre($titre);
        $this->CorpsDescription();
    }

    function AjouterParticipant($titre)
    {
        $this->TitreChapitre($titre);
        $this->CorpsParticipant();
    }

    function AjouterBudget($titre)
    {
        $this->TitreChapitre($titre);
        $this->CorpsBudget();
    }

    function AjouterCovoiturage($titre)
    {
        $this->TitreChapitre($titre);
        $this->CorpsVoiture();

    }

    function AjouterDormir($titre)
    {
        $this->TitreChapitre($titre);
        $this->CorpsDormir();
    }

    function TitreChapitre($libelle)
    {
        $this->SetFont('Arial', 'B', 12);
        $this->SetFillColor(255, 255, 255);
        $this->Cell(0, 6, utf8_decode($libelle), 0, 1, 'L', true);
        $this->Ln(2);
    }

    function CorpsDescription()
    {
        $paragraphe1 = "L'événement \"$this->nom_de_evenement\" se déroule le $this->date à $this->lieu.";
        $paragraphe2 = "Description rapide : $this->description";
        $this->SetFont('Times', '', 12);
        $this->MultiCell(0, 5, utf8_decode($paragraphe1));
        $this->MultiCell(0, 5, utf8_decode($paragraphe2));
        $this->Ln(4);
    }

    function CorpsParticipant()
    {
        $this->SetFont('Times', '', 12);
        $liste = "";
        foreach ($this->participants as $personne) {
            $liste .= "- $personne\n";
        }
        $this->MultiCell(0, 5, utf8_decode($liste));
        $this->Ln(4);
    }

    function CorpsBudget()
    {
        $this->SetFont('Times', '', 12);
        $Budget = "";
        if ($this->budget == "Non") {
            $Budget = "Option non nécéssaire.";
            $this->MultiCell(0, 5, utf8_decode($Budget));
        } else {
            if (count($this->budget) != 0) {
                $this->SetFont('Times', 'B', 12);
                $this->Cell(40, 6, "Produit", 1);
                $this->Cell(40, 6, "Prise en charge", 1);
                $this->Cell(40, 6, "Prix (en euros)", 1);
                $this->Ln();
                foreach ($this->budget as $ligne) {
                    $this->SetFont('Times', '', 12);
                    $this->Cell(40, 6, $ligne[0], 1);
                    $this->Cell(40, 6, $ligne[1], 1);
                    $this->Cell(40, 6, $ligne[2], 1);
                    $this->Ln();
                }
            } else {
                $Budget = "Option non nécéssaire.";
                $this->MultiCell(0, 5, utf8_decode($Budget));
            }
        }
        $this->Ln(4);
    }

    function CorpsVoiture()
    {
        $this->SetFont('Times', '', 12);
        if ($this->voiture == "Non") {
            $Voiture = "Option non nécéssaire.";
            $this->MultiCell(0, 5, utf8_decode($Voiture));
        } else {
            if($this->voiture == array()) {
                $msg = "Aucun participant n'a la possibilité d'utiliser une voiture";
                    $this->MultiCell(0, 5, utf8_decode($msg));
            } else {
                foreach ($this->voiture as $ligne) {
                    if ($ligne[4] != "") {
                        $Voiture = "Dans la voiture de " . $ligne[0] . " chaque passager doit payer " . $ligne[1] . " euros et le lieu de rendez-vous est :" . $ligne[4];
                    } else {
                        $Voiture = "Dans la voiture de " . $ligne[0] . " chaque passager doit payer " . $ligne[1] . " euros.";
                    }
                    $this->MultiCell(0, 5, utf8_decode($Voiture));
                    $liste = "";
                    if ($ligne[3] != "") {
                        $phrase = "Liste des passagers :";
                        $this->MultiCell(0, 5, utf8_decode($phrase));
                        $this->Ln();
                        foreach ($ligne[3] as $personne) {
                            $liste .= "- $personne\n";
                        }
                        $this->MultiCell(0, 5, utf8_decode($liste));
                    } else {
                        $phrase = "Aucun passagers";
                        $this->MultiCell(0, 5, utf8_decode($phrase));
                    }
                    $this->Ln();
                }
            }
        }
        $this->Ln(4);
    }

    function CorpsDormir()
    {
        $this->SetFont('Times', '', 12);
        if ($this->dormir == "Non") {
            $Dormir = "Option non nécéssaire.";
            $this->MultiCell(0, 5, utf8_decode($Dormir));
        } else {
            if($this->voiture == array()) {
                $msg = "Aucun participant n'a la possibilité d'héberger";
                $this->MultiCell(0, 5, utf8_decode($msg));
            } else {
                foreach ($this->dormir as $ligne) {
                    $this->SetFont('Times', '', 12);
                    $Dormir = $ligne[0] . " heberge pour " . $ligne[1] . " euros par nuit";
                    $this->MultiCell(0, 5, utf8_decode($Dormir));
                    $liste = "";
                    if ($ligne[2] != "") {
                        $phrase = "Liste des personne hébergés :";
                        $this->MultiCell(0, 5, utf8_decode($phrase));
                        $this->Ln();
                        foreach ($ligne[2] as $personne) {
                            $liste .= "- $personne\n";
                        }
                        $this->MultiCell(0, 5, utf8_decode($liste));
                    } else {
                        $phrase = "Aucune personne hébergé";
                        $this->MultiCell(0, 5, utf8_decode($phrase));
                    }
                    $this->Ln();
                }
            }
        }
        $this->Ln(4);
    }
}

?>