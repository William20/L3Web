<?php

/**
 * Created by PhpStorm.
 * User: amelie
 * Date: 21/10/16
 * Time: 14:15
 */

require_once("../include/fonctions.inc.php");

class Evenement
{
    private $nom;
    private $lieu;
    private $description;
    private $date;
    private $participant;
    private $admin;
    private $visibilite;
    private $budget;
    private $dormir;
    private $voiture;

    /**
     * Evenement constructor.
     * @param $nom
     * @param $lieu
     * @param $description
     * @param $date
     * @param array $participant
     * @param $visibilite
     * @param $nb_places_voitures
     */

    public function __construct($nom, $lieu, $description, $date, $participant, $admin, $visibilite, $budget, $voiture, $dormir)
    {
        $this->nom = $nom;
        $this->lieu = $lieu;
        $this->description = $description;
        $this->date = $date;
        $this->participant = $participant;
        $this->admin = $admin;
        $this->visibilite = $visibilite;
        $this->budget = $budget;
        $this->voiture = $voiture;
        $this->dormir = $dormir;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getLieu()
    {
        return $this->lieu;
    }

    /**
     * @param mixed $lieu
     */
    public function setLieu($lieu)
    {
        $this->lieu = $lieu;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setdescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return array
     */
    public function getParticipant()
    {
        return $this->participant;
    }

    /**
     * @param array $participant
     */
    public function setParticipant($participant)
    {
        $this->participant = $participant;
    }

    /**
     * @return mixed
     */
    public function getVisibilite()
    {
        return $this->visibilite;
    }

    /**
     * @param mixed $visibilite
     */
    public function setVisibilite($visibilite)
    {
        $this->visibilite = $visibilite;
    }

    /**
     * @return mixed
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * @param mixed $admin
     */
    public function setAdmin($admin)
    {
        $this->admin = $admin;
    }

    /**
     * @return mixed
     */
    public function getBudget()
    {
        return $this->budget;
    }

    /**
     * @param mixed $budget
     */
    public function setBudget($budget)
    {
        $this->budget = $budget;
    }

    /**
     * @return mixed
     */
    public function getDormir()
    {
        return $this->dormir;
    }

    /**
     * @param mixed $dormir
     */
    public function setDormir($dormir)
    {
        $this->dormir = $dormir;
    }

    /**
     * @return mixed
     */
    public function getVoiture()
    {
        return $this->voiture;
    }

    /**
     * @param mixed $voiture
     */
    public function setVoiture($voiture)
    {
        $this->voiture = $voiture;
    }


}