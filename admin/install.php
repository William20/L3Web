<?php
/**
 * Created by PhpStorm.
 * User: William
 * Date: 21/11/2016
 * Time: 20:55
 */

if (!isset($_COOKIE['install_finished']))
    setcookie("first_install", 1, time() + 60 * 60 * 24, '/');

if (isset($_GET['ok'])) {
    header("Refresh: 10; URL=../index.php");
}
require_once("../include/header.inc.php");
?>
    <header>
        Première installation du site
    </header>
    <div class="jumbotron">
        <?php
        if (isset($_GET['pb'])) {
            ?>
            <div class="alert-danger"> <?php echo $_GET['pb']; ?></div>
            <?php
        } else if (isset($_GET['ok'])) {
            ?>
            <div class="alert-success"> <?php echo $_GET['ok']; ?></div>
            <?php
        }
        ?>
        <div>
            <h2>Base de données :</h2>
        </div>
        <div>
            <form action="../traitement/install.php" method="post">
                <div class="form-group">
                    <label for="serveur">
                        Hôte*
                        <input class="form-control" type="text" id="serveur" name="serveur"
                               placeholder="xx.xx.xx.xx / localhost / xx.xx.fr / ..." style="width: 125%;" required/>
                    </label>
                </div>
                <div class="form-group">
                    <label for="nom">
                        Nom*
                        <input class="form-control" type="text" id="nom" name="nom" placeholder="MyDataBase" required/>
                    </label>
                </div>
                <div class="form-group">
                    <label for="utilisateur">
                        Utilisateur*
                        <input class="form-control" type="text" id="utilisateur" name="utilisateur"
                               placeholder="Jean-Luc" required/>
                    </label>
                </div>
                <div class="form-group">
                    <label for="mdp">
                        Mot de passe
                        <input class="form-control" type="password" id="mdp" name="mdp" placeholder="Mot de passe"/>
                    </label>
                </div>
                <div class="form-group">
                    <label for="port">
                        Port
                        <input class="form-control" type="number" id="port" name="port" placeholder="99999"
                               maxlength="5"/>
                    </label>
                </div>
                <div class="form-group">
                    <label for="mail">
                        L'envoi de mail est configuré et opérationnel sur le serveur
                        <input class="checkbox" type="checkbox" name="oui_mail" id="mail"/>
                    </label>
                    <div>
                        <a data-toggle="collapse" href="#mail_infos">En savoir plus ?</a>
                        <div id="mail_infos" class="alert-warning collapse">
                            Cette option est importante en cas d'inscription d'un nouvel utilisateur ou lorsque ce
                            dernier souhaite changer de mot de passe. Ne pas cocher la case si l'envoi de mail est
                            impossible depuis le serveur.
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-default btn-success" name="submit" value="Soumettre"/>
                </div>
            </form>
        </div>
    </div>
<?php
require_once("../include/footer.inc.php");