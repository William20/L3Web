/**
 * Created by william on 24/10/16.
 */
var vglobal;
var vglobalD;

function surligne(champ, erreur) {
    if (erreur)
        champ.style.backgroundColor = "#fba";
    else
        champ.style.backgroundColor = "";
}

function verifPseudo(champ) {
    var regex = /^[A-Za-z0-9_éèîïôöêûüâäà]{4,20}$/i;
    if (!regex.test(champ.value)) {
        surligne(champ, true);
        return false;
    }
    else {
        surligne(champ, false);
        return true;
    }
}

function verifEvt(champ) {
    var regex = /^[A-Za-z0-9_éèîïôöêûüâäà' ]{4,30}$/i;
    if (!regex.test(champ.value)) {
        surligne(champ, true);
        return false;
    }
    else {
        surligne(champ, false);
        return true;
    }
}

function verifMail(champ) {
    var regex = /^[a-zA-Z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$/;
    if (!regex.test(champ.value)) {
        surligne(champ, true);
        return false;
    }
    else {
        surligne(champ, false);
        return true;
    }
}

function verifMdp(champ) {
    var regex = /^[A-Za-z0-9]{4,20}$/;
    if (!regex.test(champ.value)) {
        surligne(champ, true);
        return false;
    }
    else {
        surligne(champ, false);
        return true;
    }
}

function memeMdp(champ1, champ2) {
    if (champ1.value != champ2.value) {
        surligne(champ1, true);
        return false;
    }
    else {
        surligne(champ1, false);
        return true;
    }
}

function verifForm(f) {
    var pseudoOk = verifPseudo(f.identifiant);
    var mailOk = verifMail(f.email);
    var mdpOk = verifMdp(f.mdp);
    var memesMdp = memeMdp(f.mdp, f.mdp2);


    if (pseudoOk && mailOk && mdpOk && memesMdp)
        return true;
    else {
        alert("Veuillez remplir correctement tous les champs");
        return false;
    }
}

function verifCode(champ, code) {
    if (champ == code) {
        return true;
    }
    else {
        return false;
    }
}

var stop_affichage = 1;
var numero = 1;

function dateUpdate() {

    var date_aujourdhui = document.getElementById("date_id").getAttribute("value");
    var nouvelle_date = document.getElementById("date_id").value;
    var update = document.getElementById("date_update");
    var tmp = document.createElement("p");
    var a_retirer = document.getElementById("msg");
    console.log("Auj : " + date_aujourdhui + "|| Nouvelle date : " + nouvelle_date);

    if ((date_aujourdhui > nouvelle_date || nouvelle_date == "")) {
        tmp.id = "msg";
        tmp.style = "color : red";
        tmp.innerHTML = "La date n'est pas correcte";
        surligne(document.getElementById("date_id"), true);
        if (stop_affichage) {
            update.appendChild(tmp);
        }
        stop_affichage = 0;
        console.log("Pas bonne date");
        var bouton = document.getElementById("ok").disabled = true;
    } else {
        surligne(document.getElementById("date_id"), false);
        stop_affichage = 1;
        update.removeChild(a_retirer);
        console.log("Bonne date");
        var bouton = document.getElementById("ok").disabled = false;
    }
}

function invitation() {
    var select = document.getElementById("visi_id").selectedIndex;
    var principale_div = document.getElementById("invit");
    var div_invite = document.createElement("div");
    div_invite.id = "div_invite_id";
    var html = "<label for='inviter_personne'>Inviter un membre à cet événement</label>" +
        "<div id='content'>" +
        "<input id='+' onclick = 'add();' type='button' value='+'/>" +
        "<input  id='fonction_a_changer' onclick = 'supp();' type='button' value='-'/>" +
        "<div id='form" + numero + "'>" +
        "<input onblur='verifUtilisateur(this, this.value,\"invit\",\"ok\");' type='text' name='inviter_personne" + numero + "' id='inviter_personne" + numero + "' placeholder='Pseudo utilisateur' required/>" +
        "</div>" +
        "</div>";
    div_invite.innerHTML = html;
    if (select == 1) {
        principale_div.appendChild(div_invite);
    }
    else if (select == 0) {
        principale_div.removeChild(document.getElementById("div_invite_id"));
        var a_retirer = document.getElementById("msg_invit");
        var update = document.getElementById("invit");
        invit.removeChild(a_retirer);
    }
}

function add() {
    ++numero;
    var champs = "<input onblur='verifUtilisateur(this, this.value,\"invit\",\"ok\");' type='text' name='inviter_personne" + numero + "' id='inviter_personne" + numero + "' placeholder='Pseudo utilisateur' required/>";
    var parent = document.getElementById("content");
    var nouveau_input = document.createElement("div");
    nouveau_input.id = "form" + numero;
    nouveau_input.innerHTML = champs;
    parent.appendChild(nouveau_input);
    document.getElementById("fonction_a_changer").onclick = function () {
        supp();
    };
}

function supp() {
    var parent = document.getElementById("content");
    var child = document.getElementById("form" + numero);
    if (numero > 1) {
        parent.removeChild(child);
        numero--;
    }
}

var compteur = 0;

function verifUtilisateur(champ, value, id, ok) {
    var xhr = getXhr();
    xhr.open("POST", "../ajax/verifications/verifUtilisateur.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send("Nom_Utilisateur=" + value);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            leselect = xhr.responseText;
            var update = document.getElementById(id);
            var tmp = document.createElement("p");
            var a_retirer = document.getElementById("msg_invit");
            var ajouter = document.getElementById(ajouter);
            var valider = document.getElementById(ok);
            if (leselect == "1") {
                surligne(champ, true);
                ajouter.disabled = true;
                valider.disabled = true;
                tmp.style = "color : red";
                tmp.id = "msg_invit";
                tmp.innerHTML = "Cet utilisateur n'existe pas ou n'a pas activé son compte";
                if (compteur == 0) {
                    update.appendChild(tmp);
                    compteur = 1;
                }
            } else {
                surligne(champ, false);
                ajouter.disabled = false;
                valider.disabled = false;
                update.removeChild(a_retirer);
                compteur = 0;
            }
        }
    }

}


var compteur2 = 0;

function verifUtilisateur2(champ, value, id, ok) {
    var xhr = getXhr();
    xhr.open("POST", "../ajax/verifications/verifUtilisateur.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    var nomE = document.getElementById("myHeader").textContent;
    xhr.send("Nom_Utilisateur=" + value + "&participant=" + nomE);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            leselect = xhr.responseText;
            var update = document.getElementById(id);
            var tmp = document.createElement("p");
            var a_retirer = document.getElementById("msg_invit");
            var valider = document.getElementById(ok);
            console.log(leselect);
            if (leselect == "1") {
                surligne(champ, true);
                valider.disabled = true;
                tmp.style = "color : red";
                tmp.id = "msg_invit";
                tmp.innerHTML = "Cet utilisateur n'existe pas ou n'a pas activé son compte ou est déjà un participant";
                if (compteur2 == 0) {
                    update.appendChild(tmp);
                    compteur2 = 1;
                }
            } else {
                surligne(champ, false);
                valider.disabled = false;
                update.removeChild(a_retirer);
                compteur2 = 0;
            }
        }
    }

}


var compteurEvent = 0;

function verifEvenement(champ, value) {

    var update = document.getElementById("NameEvent");
    var tmp = document.createElement("p");
    var a_retirer = document.getElementById("msg_Event");
    var valider = document.getElementById("ok");

    while (!verifEvt(champ)) {
        valider.disabled = true;
        tmp.style = "color : red";
        tmp.id = "msg_Event";
        tmp.innerHTML = "Ce nom n'est pas correct (vous n'avez pas le droit aux caractères spéciaux et ce nom doit contenir entre 4 et 30 caractères)";
        if (compteurEvent == 0) {
            update.appendChild(tmp);
            compteurEvent = 1;
        } else {
            valider.disabled = false;
            update.removeChild(a_retirer);
            compteurEvent = 0;
        }
    }
    var xhr = getXhr();
    xhr.open("POST", "../ajax/verifications/verifEvenement.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send("Nom_Evenement=" + value);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            leselect = xhr.responseText;
            if (leselect == "1") {
                console.log(value);
                surligne(champ, true);
                valider.disabled = true;
                tmp.style = "color : red";
                tmp.id = "msg_Event";
                tmp.innerHTML = "Ce nom est déjà utilisé";
                if (compteurEvent == 0) {
                    update.appendChild(tmp);
                    compteurEvent = 1;
                }
            } else {
                surligne(champ, false);
                valider.disabled = false;
                update.removeChild(a_retirer);
                compteurEvent = 0;
            }
        }
    }
}
function getXhr() {
    var xhr = null;
    if (window.XMLHttpRequest) // Firefox et autres
        xhr = new XMLHttpRequest();
    else if (window.ActiveXObject) { // Internet Explorer
        try {
            xhr = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
    }
    else { // XMLHttpRequest non supporté par le navigateur
        alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
        xhr = false;
    }
    return xhr;
}


function ajoutVoiture(nom, pseudo, personne, place, prix) {
    var xhr = getXhr();
    xhr.open("POST", "../ajax/voiture/ajoutVoiture.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    var element = document.getElementById(nom);
    var nomE = document.getElementById("myHeader").textContent;
    var conducteur = personne;
    var passager = pseudo;
    for (var iter = 1; iter <= place; iter++) {
        window["place_" + iter] = document.getElementById(personne + "V_" + iter).textContent;
    }
    var comparatif = "place_" + nom;
    var tmp = 0;
    for (var iter = 1; iter <= place; iter++) {
        if (window["place_" + iter] == pseudo && comparatif != "place_" + personne + "V_" + iter) {
            tmp = 0;
            break;
        } else {
            tmp = 1;
        }
    }
    if (tmp == 1) {
        if (element.textContent == "" && vglobal != 1) {
            element.innerHTML += pseudo;
            vglobal = 1;
            xhr.send("Nom_E=" + nomE + "&Conducteur=" + conducteur + "&Passager=" + passager + "&Prix=" + prix);
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    leselect = xhr.responseText;
                    console.log(leselect);
                }
            }
        } else {
            if (element.textContent == pseudo) {
                element.textContent = "";
                vglobal = 0;
                xhr.send("Nom_E=" + nomE + "&Conducteur=" + conducteur + "&Passager=" + passager + "&Prix=" + prix + "&Del=1");
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.status == 200) {
                        leselect = xhr.responseText;
                        console.log(leselect);
                    }
                }
            }
        }
    }
}

function virerVoiture(nomU, nomAVirer) {
    var xhr = getXhr();
    xhr.open("POST", "../ajax/voiture/virerVoiture.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    var nomE = document.getElementById("myHeader").textContent;
    var element = document.getElementById(nomAVirer);
    var passager = element.textContent;
    element.textContent = "";
    xhr.send("Nom_E=" + nomE + "&Conducteur=" + nomU + "&Passager=" + passager);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            leselect = xhr.responseText;
            console.log(leselect);
        }
    }
}

function ajoutDormir(nom, pseudo, personne, place, prix) {
    console.log("tez");
    var xhr = getXhr();
    xhr.open("POST", "../ajax/dormir/ajoutDormir.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    var element = document.getElementById(nom);
    var nomE = document.getElementById("myHeader").textContent;
    var hebergeur = personne;
    var gens = pseudo;
    for (var iter3 = 1; iter3 <= place; iter3++) {
        window["placeD_" + iter3] = document.getElementById(personne + "D_" + iter3).textContent;
    }
    var comparatif = "placeD_" + nom;
    var tmp = 0;
    for (var iter3 = 1; iter3 <= place; iter3++) {
        if (window["placeD_" + iter3] == pseudo && comparatif != "placeD_" + personne + "D_" + iter3) {
            tmp = 0;
            break;
        } else {
            tmp = 1;
        }
    }
    if (tmp == 1) {
        if (element.textContent == "" && vglobalD != 1) {
            element.innerHTML += pseudo;
            vglobalD = 1;
            xhr.send("Nom_ED=" + nomE + "&hebergeur=" + hebergeur + "&gens=" + gens + "&Prix=" + prix);
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    leselect = xhr.responseText;
                    console.log(leselect);
                }
            }
        } else {
            if (element.textContent == pseudo) {
                element.textContent = "";
                vglobalD = 0;
                xhr.send("Nom_ED=" + nomE + "&hebergeur=" + hebergeur + "&gens=" + gens + "&Prix=" + prix + "&DelD=1");
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.status == 200) {
                        leselect = xhr.responseText;
                        console.log(leselect);
                    }
                }
            }
        }
    }
}

function virerDormir(nomU, nomAVirer) {
    var xhr = getXhr();
    xhr.open("POST", "../ajax/dormir/virerDormir.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    var nomE = document.getElementById("myHeader").textContent;
    var element = document.getElementById(nomAVirer);
    var passager = element.textContent;
    element.textContent = "";
    xhr.send("Nom_E=" + nomE + "&hebergeur=" + nomU + "&gens=" + passager);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            leselect = xhr.responseText;
            console.log(leselect);
        }
    }
}

var voiture = 0;
function parametreVoiture() {
    var element = document.getElementById("Voiture");
    var add = document.createElement("div");
    add.id = "div_add_id";
    var champ = '' +
        '<div class="form-group" id="div_add_id">' +
        '   <label for="place">' +
        '   Nombre de personnes que vous pouvez transporter (sans vous compter) :' +
        '       <input class="form-control" id="place" name="place"' +
        '           value=""' +
        '           type="number" required/>' +
        '    </label>' +
        '    <label for="prix">' +
        '       Fixez un prix (en euros) par personne pour un trajet moyen' +
        '       (environ 15 minutes de temps de trajet) :' +
        '       <input class="form-control" id="prix" name="prix"' +
        '       value=""' +
        '       type="number" required/>' +
        '    </label>' +
        '</div>';
    add.innerHTML = champ;
    if (voiture == 0) {
        voiture = 1;
        element.appendChild(add);
    }
}

function retirerPar() {
    var element = document.getElementById("Voiture");
    var add = document.getElementById("div_add_id");
    element.removeChild(add);
    voiture = 0;

}

var dormir = 0;
function parametreDormir() {
    var element = document.getElementById("Dormir");
    var add = document.createElement("div");
    add.id = "div_add_idD";

    var champ = '' +
        '<div class="form-group">' +
        '   <label for="placeD">' +
        '       Combien de personnes pouvez-vous héberger (sans vous compter) :' +
        '       <input class="form-control" id="placeD" name="placeD"' +
        '           value="" type="number" required/>' +
        '   </label>' +
        '   <br />' +
        '   <label for="prixD">' +
        '       Fixez un prix (en euros) par personne pour une nuit :' +
        '       <input class="form-control" id="prixD" name="prixD"' +
        '           value="" type="number" required/>' +
        '   </label>' +
        '</div>';

    add.innerHTML = champ;
    if (dormir == 0) {
        dormir = 1;
        element.appendChild(add);
    }
}

function retirerParD() {
    var element = document.getElementById("Dormir");
    var add = document.getElementById("div_add_idD");
    element.removeChild(add);
    dormir = 0;
}

function addObjt(champ) {
    var add = champ.value;
    var id = champ.id;
    var xhr = getXhr();
    var nom = document.getElementById("myHeader").textContent;
    xhr.open("POST", "../ajax/budget/addObjt.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send("IdObjt=" + id + "&ValueObjt=" + add + "&NomEvt=" + nom);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            leselect = xhr.responseText;
            console.log(leselect);
        }
    }
}

function addPrise(champ) {
    var add = champ.value;
    var id = champ.id;
    var xhr = getXhr();
    var nom = document.getElementById("myHeader").textContent;
    xhr.open("POST", "../ajax/budget/addPrise.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send("IdObjt=" + id + "&PriseObjt=" + add + "&NomEvt=" + nom);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            leselect = xhr.responseText;
            console.log(leselect);
        }
    }
}

function addPrix(champ) {
    var add = champ.value;
    var id = champ.id;
    var xhr = getXhr();
    var nom = document.getElementById("myHeader").textContent;
    xhr.open("POST", "../ajax/budget/addPrix.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send("IdObjt=" + id + "&PrixObjt=" + add + "&NomEvt=" + nom);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            leselect = xhr.responseText;
            console.log(leselect);
        }
    }
}

var globalLigneB;

function suppLigneB(ligneB) {
    if (globalLigneB == null) {
        globalLigneB = ligneB;
    }
    if (globalLigneB > 1) {
        document.getElementById("budget").deleteRow(globalLigneB);
        var nom = document.getElementById("myHeader").textContent;
        var xhr = getXhr();
        xhr.open("POST", "../ajax/budget/suppLigneB.php", true);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.send("nbLigne=" + globalLigneB + "&NomEvt=" + nom);
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                leselect = xhr.responseText;
                console.log(leselect);
            }
        }
        globalLigneB--;
    }
}

function addLigneB(ligneB) {
    console.log(ligneB);
    console.log(globalLigneB);
    var table = document.getElementById("budget");
    var add = document.createElement("tr");
    if (globalLigneB == null) {
        globalLigneB = ligneB;
    }
    globalLigneB++;
    add.id = globalLigneB;
    console.log(globalLigneB);
    console.log(ligneB);
    var html = "<td class='tdthBudget'><input onblur='addObjt(this)'; type='text' id='" + globalLigneB + "' name='objet" + globalLigneB + "' placeholder='Bouteille' required></td>" +
        "<td class='tdthBudget'><input onblur='addPrise(this);' type='text' id='" + globalLigneB + "' name='prise" + globalLigneB + "' placeholder='Jean'></td>" +
        "<td class='tdthBudget'><input onblur='addPrix(this);' type='number' id='" + globalLigneB + "' name='prix" + globalLigneB + "'placeholder='2'></td>";

    add.innerHTML = html;
    table.appendChild(add);
}

function SuppB() {
    var table = document.getElementById("budgetDiv");
    var nom = document.getElementById("myHeader").textContent;
    var xhr = getXhr();
    xhr.open("POST", "../ajax/budget/suppB.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send("DelE=1&NomEvt=" + nom);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            leselect = xhr.responseText;
            console.log(leselect);
        }
    }

    var html = '' +
        '<div class="form-group" id=budgetDiv>' +
        '   <label for="budget">Gestion du budget :</label>' +
        '   <input class="btn btn-default" onclick="AddB();" ' +
        '       type="button" name="suppB" id="suppB" value="Activer l\'option"/>' +
        '</div>';
    table.innerHTML = html;
}

function AddB() {
    var table = document.getElementById("budgetDiv");
    var nom = document.getElementById("myHeader").textContent;
    var xhr = getXhr();
    xhr.open("POST", "../ajax/budget/addB.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send("supp=1&NomEvt=" + nom);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
            var tmp = xhr.responseText;
            console.log(tmp);
            table.innerHTML = tmp;
        }
    }
}

function SuppV() {
    var table = document.getElementById("voitureDiv");
    var nom = document.getElementById("myHeader").textContent;
    var xhr = getXhr();
    xhr.open("POST", "../ajax/voiture/suppV.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send("DelV=1&NomEvt=" + nom);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            leselect = xhr.responseText;
            console.log(leselect);
        }
    }
    var html = "<label>Gestion du covoiturage :</label>" +
        "<input class='btn btn-default' onclick='AddV();' value=\"Activer l\'option\" id='activer' type='button' style='margin-top: 3%' >";
    table.innerHTML = html;
}

function AddV() {
    var table = document.getElementById("voitureDiv");
    var nom = document.getElementById("myHeader").textContent;
    var xhr = getXhr();
    xhr.open("POST", "../ajax/voiture/addV.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send("suppV=1&NomEvt=" + nom);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
            var tmp = xhr.responseText;
            table.innerHTML = tmp;
            console.log("test" + nom + "k");
        }

    }

}

function SuppD() {
    var table = document.getElementById("dormirDiv");
    var nom = document.getElementById("myHeader").textContent;
    var xhr = getXhr();
    xhr.open("POST", "../ajax/dormir/suppD.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send("DelD=1&NomEvt=" + nom);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            leselect = xhr.responseText;
            console.log(leselect);
        }
    }
    var html = "<label>Qui dort où ?</label>" +
        "<input class='btn btn-default' onclick='AddD();' value=\"Activer l\'option\" id='activer' type='button' style='margin-top: 3%' >";
    table.innerHTML = html;
}

function AddD() {
    var table = document.getElementById("dormirDiv");
    var nom = document.getElementById("myHeader").textContent;
    var xhr = getXhr();
    xhr.open("POST", "../ajax/dormir/addD.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send("suppD=1&NomEvt=" + nom);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
            var tmp = xhr.responseText;
            table.innerHTML = tmp;
        }
    }
}

function suppParticipant(participant) {
    var nom = document.getElementById("myHeader").textContent;
    var participantID = participant.id;
    var liste = document.getElementById("participants");
    var liASupp = document.getElementById("li_" + participantID)
    liste.removeChild(liASupp);
    var xhr = getXhr();
    xhr.open("POST", "../ajax/participants/suppParticipant.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send("utilisateur=" + participantID + "&NomEvt=" + nom);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
            var tmp = xhr.responseText;
            console.log(tmp);
        }
    }
}

function addChampParticipant() {

    var nom = document.getElementById("myHeader").textContent;
    var divAadd = document.getElementById("Liste_Participants");
    var divInput = document.createElement("div");
    divInput.id = "divInput";
    divInput.class = "form-group";
    var html = "<label for='addParti_id'>Entrer le pseudo de l'utilisateur à ajouter :<input class='form-control' oninput='verifUtilisateur2(this,this.value,\"Liste_Participants\",\"submit_participant\")' type='text' id='addParti_id' maxlength='40' required/></label>" +
        "<button onclick='addParticipant(document.getElementById(\"addParti_id\").value);' class='btn-default btn' type='button' id='submit_participant' disabled>Ok</button>" +
        "<button onclick='cancelParticipant();' class='btn-default btn' type='button' >Annuler</button>";
    divInput.innerHTML = html;
    if (document.getElementById("divInput") == null)
        divAadd.appendChild(divInput);
}

function addParticipant(participant) {
    var nom = document.getElementById("myHeader").textContent;
    var divAadd = document.getElementById("Liste_Participants");
    var divAsupp = document.getElementById("divInput");
    var divParti = document.getElementById("participants");
    var li = document.createElement("li");
    li.id = "li_"+participant;
    var x = document.createElement("INPUT");
    x.setAttribute("type", "button");
    x.setAttribute("onclick", "suppParticipant(this);");
    x.setAttribute("class", "btn-circle btn-danger btn");
    x.setAttribute("id", participant);
    x.setAttribute("value", "X");
    var html  = participant;
    divAadd.removeChild(divAsupp);
    var xhr = getXhr();
    xhr.open("POST", "../ajax/participants/addParticipant.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send("utilisateur=" + participant + "&NomEvt=" + nom);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
            var tmp = xhr.responseText;
            li.appendChild(document.createTextNode(html));
            li.appendChild(x);
            divParti.appendChild(li);
            console.log(tmp);
        }
    }
}

function cancelParticipant() {
    var nom = document.getElementById("myHeader").textContent;
    var divAadd = document.getElementById("Liste_Participants");
    var divAsupp = document.getElementById("divInput");
    var msg = document.getElementById("msg_invit");
    divAadd.removeChild(divAsupp);
    divAadd.removeChild(msg);
}

function setRDV(rdv,nom){
    var xhr = getXhr();
    xhr.open("POST", "../ajax/participants/modifRDV.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send("rdv=" + rdv + "&NomUti=" + nom);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
            var tmp = xhr.responseText;
            console.log(tmp);
        }
    }
}