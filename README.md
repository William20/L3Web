L3 Web Project

Le site est disponible à l'adresse suivante : 
http://comateincorporation.hol.es/

Instalation et utilisation :

Dézippez le dossier du site CoMateIncorporation et mettez le sur le serveur.

Première installation :

    Vous devez commencer par confgurer la base de données. 
    Lancez votre navigateur et depuis le dossier racine de votre serveur, allez sur la page : /admin/install.php
    Remplissez le formulaire pour configurer la base de données. 
    Vous serez redirigé vers la page d'accueil et vous pourrez utiliser le site !
    
Utilisation générale :
    
    Pour naviguer sur le site vous devez avoir un compte qu'il est possible de créer depuis la page d'accueil.
    Ensuite, connectez-vous via les informations rentrées lors de l'inscription et voilà ! Bonne navigation !
    
Si vous avez des problèmes d'installation ou de configuration contactez les administrateurs :

- Amélie Rémond => amelie.remond@orange.fr
- William Rozenberg => william.rozenberg@gmail.com